from gmb.environment import Environment


def get_db_config():
    cnf = Environment.get().static_config
    try:
        port = int(cnf.mysql_db_port)
    except ValueError as err:
        print("The mysql port must be an integer")
        raise err
    return {
        "host": cnf.mysql_db_host,
        "username": cnf.mysql_db_username,
        "password": cnf.mysql_db_password,
        "database": cnf.mysql_db_database,
        "port": port
    }


def get_db_test_config():
    cnf = Environment.get().static_config
    try:
        port = int(cnf.mysql_test_db_port)
    except ValueError as err:
        print("The mysql test port must be an integer")
        raise err
    return {
        "host": cnf.mysql_test_db_host,
        "username": cnf.mysql_test_db_username,
        "password": cnf.mysql_test_db_password,
        "database": cnf.mysql_test_db_database,
        "port": port
    }
