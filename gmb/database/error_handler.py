import time
import traceback
from typing import Callable, List, Tuple, Type

from peewee import OperationalError, ProgrammingError

from _thread import interrupt_main  # type: ignore
from gmb.environment import Environment

from .database import DatabaseSingleton
from .schema import Schema


def __operational_error_handler(error: OperationalError) -> None:
    print("Handling operational error.")
    code, msg = error.args
    if code == 1927:
        # Connection was killed
        print("Handling connection was killed.")
        print("Sleeping 10 seconds before reinitialising database.")
        time.sleep(10)
        DatabaseSingleton.initialize_default()
    if code == 2013:
        # Connection was killed
        print("Handling lost MySQL connection during query.")
        print("Sleeping 10 seconds before reinitialising database.")
        time.sleep(10)
        DatabaseSingleton.initialize_default()
    elif code == 2006:
        print("Handling MySQL server gone.")
        print("Reinitialising database.")
        # MySQL server has gone away
        DatabaseSingleton.initialize_default()


def __get_peewee_error_handlers(
) -> List[Tuple[Type[Exception], Callable[[Exception], None]]]:
    return [(OperationalError, __operational_error_handler)]


def __error_callback(error: Exception) -> bool:
    """Returns true if error is fixed."""
    print("An error occurred: {}".format(error))
    peewee_err = __get_peewee_error_handlers()
    for ex, exfunc in peewee_err:
        if isinstance(error, ex):
            print("Trying to fix the error.")
            exfunc(error)
            return True
    print("No error handler found.")
    return False


def handle_errors(callback: Callable):
    def deco(*args, **kwargs):
        try:
            ret = callback(*args, **kwargs)
            return ret
        except Exception as error:
            fixed = __error_callback(error)
            if not fixed:
                raise error
        # after all tries, try again with killing main
        try:
            ret = callback(*args, **kwargs)
            print("Error fix succeeded.")
            return ret
        except Exception as error:
            print("Unable to fix error. Exiting main!")
            interrupt_main()
            raise error

    return deco
