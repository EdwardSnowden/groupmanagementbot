from peewee import Database, MySQLDatabase, Proxy

from .util import get_db_config


class DatabaseSingleton:
    """Connection to a mysql database."""

    __instance = Proxy()

    @staticmethod
    def get() -> Proxy:
        return DatabaseSingleton.__instance

    @staticmethod
    def initialize(database: Database):
        DatabaseSingleton.__instance.initialize(database)

    @staticmethod
    def initialize_mysql(host: str,
                         username: str,
                         password: str,
                         database: str,
                         port: int = 3306):
        database = MySQLDatabase(host=host,
                                 user=username,
                                 password=password,
                                 database=database,
                                 port=port,
                                 charset='utf8mb4')
        DatabaseSingleton.initialize(database)

    @staticmethod
    def initialize_default():
        DatabaseSingleton.initialize_mysql(**get_db_config())
