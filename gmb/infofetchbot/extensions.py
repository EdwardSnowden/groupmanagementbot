from __future__ import annotations

from typing import Type

from gmb.abstract_extensions import Extensions

from .extension import InfoFetchExt


class InfoFetchExtensions(Extensions):
    pass


def register_infofetch_extension(cls: Type[InfoFetchExt]
                                 ) -> Type[InfoFetchExt]:
    print("Registered infofetch extension: {}".format(cls.__name__))
    InfoFetchExtensions.add_extension(cls)
    return cls
