"""This module contains the netiquette extention."""

from __future__ import annotations

from dataclasses import dataclass
from typing import List

import requests
from bs4 import BeautifulSoup
from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.custom_message.utils import send_custom_reply
from gmb.silent_mode import silent_mode

from .netiquette_parser import NetiquetteFetcher


@register_pollingbot_extension
class NetiquetteExt(PollBotExt):
    """An extension that shows the netiquette."""
    def get_handlers(self) -> List[PriorityHandler]:
        """Returns a list with the netiquette PriorityHandlers."""
        return [
            PriorityHandler(CommandHandler('netiquette', self.netiquette),
                            HandlerPriority.NORMAL),
        ]

    @silent_mode
    def netiquette(self, bot: Bot, update: Update) -> None:
        """The /netiquette command.

        Shows the netiquette.
        """
        msg = NetiquetteFetcher.get_netiquette()
        send_custom_reply(msg, bot, update)
