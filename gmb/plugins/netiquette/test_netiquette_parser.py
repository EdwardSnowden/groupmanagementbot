"""Unittests for the module netiquette_parser."""

import unittest

from bs4 import BeautifulSoup
from ddt import data, ddt, unpack

from gmb.config.config import StaticConfig
from gmb.environment import Environment

from .netiquette_parser import (NetiquetteDownloadException, NetiquetteFetcher,
                                get_depth)


class TestGetDepth(unittest.TestCase):
    """Unittests for the get_depth method."""
    def test_get_depth(self):
        """Tests if get_depth returns the correct depths."""
        string1 = "<ul><li></li></ul>"
        soup = BeautifulSoup(string1, 'lxml')
        deepness = get_depth(soup.li)
        self.assertEqual(deepness, 1)
        string2 = "<ul><li><ul><li></li></ul></li></ul>"
        soup = BeautifulSoup(string2, 'lxml')
        deepness = get_depth(soup.li)
        self.assertEqual(deepness, 1)
        deepness = get_depth(soup.li.li)
        self.assertEqual(deepness, 2)
        string3 = "<ul><li><ul><li><ul><li></li></ul></li></ul></li></ul>"
        soup = BeautifulSoup(string3, 'lxml')
        deepness = get_depth(soup.li)
        self.assertEqual(deepness, 1)
        deepness = get_depth(soup.li.li)
        self.assertEqual(deepness, 2)
        deepness = get_depth(soup.li.li.li)
        self.assertEqual(deepness, 3)


class TestNetiquetteFetcher(unittest.TestCase):
    """Unittests for the NetiquetteFetcher class."""
    def test_download_netiquette(self):
        """Tests if download netiquette downloads a netiquette without errors."""
        link = "https://pad.fridaysforfuture.de/p/FFFGMBBOTTESTNETIQUETTE/export/html"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        netiquette = NetiquetteFetcher.download_netiquette()
        self.assertTrue(netiquette is not None)
        self.assertTrue(netiquette != "")

    def test_download_netiquette_exceptions(self):
        """Tests all errors on downloading the netiquette."""
        # no link
        link = None
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        with self.assertRaises(NetiquetteDownloadException):
            NetiquetteFetcher.download_netiquette()
        # wrong schema
        link = "pad.fridaysforfuture.de/p/FFFGMBBOTNOTEXISTING/export/html"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        with self.assertRaises(NetiquetteDownloadException):
            NetiquetteFetcher.download_netiquette()
        # not existing pad
        link = "https://pad.fridaysforfuture.de/p/FFFGMBBOTNOTEXISTING/export/html"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        with self.assertRaises(NetiquetteDownloadException):
            NetiquetteFetcher.download_netiquette()
        link = "https://beispiel.de"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        with self.assertRaises(NetiquetteDownloadException):
            NetiquetteFetcher.download_netiquette()

    def test_get_netiquette(self):
        """Tests if get_netiquette downloads the netiquette and if the cache works."""
        link = "https://pad.fridaysforfuture.de/p/FFFGMBBOTTESTNETIQUETTE/export/html"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        NetiquetteFetcher.get_netiquette()  # download
        netiquette_update = NetiquetteFetcher.netiquette_update
        NetiquetteFetcher.get_netiquette()  # use cache
        netiquette_update_cache = NetiquetteFetcher.netiquette_update
        self.assertEqual(netiquette_update, netiquette_update_cache)
        netiquette_update.next_update = 0
        NetiquetteFetcher.get_netiquette()  # use cache
        netiquette_update_neu = NetiquetteFetcher.netiquette_update
        self.assertNotEqual(netiquette_update, netiquette_update_neu)

    def test_update_netiquette(self):
        """Tests if the update_netiquette method works."""
        # new download
        link = "https://pad.fridaysforfuture.de/p/FFFGMBBOTTESTNETIQUETTE/export/html"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        netiquette = NetiquetteFetcher.update_netiquette()
        self.assertTrue(netiquette.netiquette is not None)
        self.assertTrue(netiquette.netiquette != "")
        # test if it updates
        netiquette_neu = NetiquetteFetcher.update_netiquette()
        self.assertNotEqual(netiquette, netiquette_neu)
        # netiquette download exception
        link = "https://beispiel.de"
        sc = StaticConfig(netiquette_link=link)
        Environment._Environment__env = Environment(sc)
        netiquette = NetiquetteFetcher.update_netiquette()
        self.assertEqual("An error occured.", netiquette.netiquette)
