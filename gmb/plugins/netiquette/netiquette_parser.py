"""Contains all methods and classes that are needed to get the current netiquette."""

import re
from dataclasses import dataclass
from time import time
from typing import Optional

import requests
from bs4 import BeautifulSoup

from gmb.environment import Environment
from gmb.pager.parser import HTMLParser


def get_depth(tag) -> int:
    """Count deepness level of lists."""
    if not tag:
        return 0
    if tag.name == "ul":
        return get_depth(tag.parent) + 1
    return get_depth(tag.parent) + 0


@dataclass
class NetiquetteUpdate:
    """A dataclass that contains a string and the time this string should be updated."""
    netiquette: str
    next_update: float


class NetiquetteDownloadException(Exception):
    """Is thrown, when the download of the netiquette doesn't work."""


class NetiquetteFetcher():
    """Fetcher for the netiquette.

    Uses cache to not download the netiquette everytime.
    """

    netiquette_update: Optional[NetiquetteUpdate] = None

    @staticmethod
    def update_netiquette() -> NetiquetteUpdate:
        """Updates the netiquette, saves and returns the new netiquette.

        Also keeps track of the error handling and saves error messages as netiquette message."""
        try:
            netiquette = NetiquetteFetcher.download_netiquette()
            next_update = time() + (60 * 60)
        except NetiquetteDownloadException as er:
            netiquette = er.args[0]
            next_update = time() + (60 * 60)
        NetiquetteFetcher.netiquette_update = NetiquetteUpdate(
            netiquette, next_update)
        return NetiquetteFetcher.netiquette_update

    @staticmethod
    def get_netiquette() -> str:
        """Returns the netiquette.

        Updates the netiquette if the cached one is older than 1h.
        """
        if NetiquetteFetcher.netiquette_update is None:
            NetiquetteFetcher.netiquette_update = NetiquetteFetcher.update_netiquette(
            )
        elif NetiquetteFetcher.netiquette_update.next_update < time():
            NetiquetteFetcher.netiquette_update = NetiquetteFetcher.update_netiquette(
            )
        return NetiquetteFetcher.netiquette_update.netiquette

    @staticmethod
    def download_netiquette() -> str:
        """Downloads the netiquette and returns it as string."""
        link = Environment.get().static_config.netiquette_link
        if not link:
            print(
                "Netiquette is not configured. To activate it add the link to the etherpad html export of the netiquette to the secrets or environment variable config."
            )
            raise NetiquetteDownloadException("Netiquette is not configured.")
        print("Downloading netiquette.")
        try:
            r = requests.get(link)
        except requests.exceptions.MissingSchema:
            print(
                "Netiquette is not configured correctly. The link must start with http or https."
            )
            raise NetiquetteDownloadException(
                "Netiquette is not configured correctly.")
        text = r.text.replace("<br/>", "\n").replace("<br>", "\n")
        soup = BeautifulSoup(text, 'lxml')
        if re.search("<pre>Cannot GET /p/(.*)/export/html</pre>",
                     str(soup.body.pre)):
            print(
                "Netiquette is not configured correctly. The netiquette doesn't exist under the configured link."
            )
            raise NetiquetteDownloadException(
                "Netiquette is not configured correctly.")
        # remove comments
        a = soup.find("div", id='comments')
        if a is not None:
            a.decompose()
        # remove empty lists
        for a in soup.findAll("li"):
            if not a.contents:
                a.decompose()
        for a in soup.findAll("ul"):
            if not a.contents:
                a.insert_before("\n")
                a.decompose()
        # replace lists with chars
        for a in soup.findAll("ul"):
            for b in a.findAll("li"):
                depth = get_depth(b)
                b.insert_before("\n{} ".format("-" * depth))
                b.unwrap()
            a.insert_after("\n")
            a.unwrap()
        for tag in range(1, 7):
            for a in soup.findAll(f"h{tag}"):
                replacement = soup.new_tag("b")
                replacement.string = a.string
                a.replace_with(replacement)
        out = str(soup.body).replace("<body>", "").replace("</body>", "")
        out = re.sub("\n\*\s*(?!<strong>)\s*(.*)\s*(?!</strong>)\s*\n",
                     "\n<strong>\\1</strong>\n", out)
        out = re.sub("\n\*\s*<strong>\s*(.*)\s*</strong>\s*\n",
                     "\n<strong>\\1</strong>\n", out)
        out = out.replace("\n\n\n", "\n\n")
        try:
            # test if html is valid
            HTMLParser.parse(out)
        except Exception as e:
            print(
                "Could not parse data from netiquette link. The link is configured wrong or the html contains tags that aren't allowed. If the link is correct, contact a maintainer of this bot."
            )
            print(e)
            raise NetiquetteDownloadException("An error occured.")
        return out
