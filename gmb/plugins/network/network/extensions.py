from typing import Type, Optional

from gmb.abstract_extensions import Extensions

from .extension import NetworkExt


class NetworkExtensions(Extensions):
    """Contains all network extensions."""
    @classmethod
    def get_last(cls) -> Optional[Type[NetworkExt]]:
        """Returns the last network extension, that is in the extensions list.

        When the list is empty none will be returned.
        """
        ext = cls.get_extensions()
        if len(ext) >= 1:
            return ext[len(ext) - 1]
        else:
            return None


def register_network_extension(cls: Type[NetworkExt]) -> Type[NetworkExt]:
    """Add a network extension to the network extensions.

    Notice:
        Only the last one added is used.
    """
    print("Registered network extension: {}".format(cls.__name__))
    NetworkExtensions.add_extension(cls)
    return cls
