from gmb.api.database import (register_non_persistent,
                              register_non_persistent_with_dependencies,
                              register_system_config)
from gmb.api.datastructs import (AbstractChat, AbstractChatAdmin,
                                 AbstractChatsConfig, AbstractUser,
                                 chat_admin_class)


@register_system_config
class NetworkChatsConfig(AbstractChatsConfig):
    class Meta:
        table_name = "chatsconfig"


@register_non_persistent
class NetworkChat(AbstractChat):
    class Meta:
        table_name = "chat"


@register_non_persistent
class NetworkUser(AbstractUser):
    class Meta:
        table_name = "user"


@register_non_persistent_with_dependencies(NetworkChat, NetworkUser)
@chat_admin_class(NetworkUser, NetworkChat, "chat_admin")
class NetworkChatAdmin(AbstractChatAdmin):
    pass
