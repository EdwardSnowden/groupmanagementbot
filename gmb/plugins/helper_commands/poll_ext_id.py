from __future__ import annotations

from typing import List

from telegram import Bot, Update, Message
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.custom_message.utils import send_custom_reply
from gmb.silent_mode import silent_mode


@register_pollingbot_extension
class IdExt(PollBotExt):
    """Extension for the id command"""
    def get_handlers(self: IdExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('id', self.get_id),
                            HandlerPriority.NORMAL),
        ]

    @silent_mode
    def get_id(self: IdExt, bot: Bot, update: Update) -> None:
        """Sends the id of the message, user and chat.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        user_id = update.effective_user.id
        chat_id = update.effective_chat.id
        msg_id = update.effective_message.message_id
        msg = f"User-ID: {user_id}"
        msg += f"\nChat-ID: {chat_id}"
        msg += f"\nMessage-ID: {msg_id}"
        reply_message: Message = update.message.reply_to_message
        if reply_message:
            rep_user_id = reply_message.from_user.id
            rep_msg_id = reply_message.message_id
            msg += f"\nReply-User-ID: {rep_user_id}"
            msg += f"\nReply-Message-ID: {rep_msg_id}"
        send_custom_reply(msg, bot, update)
