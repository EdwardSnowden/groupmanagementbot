from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.custom_message.utils import send_custom_reply
from gmb.telegram_util import only_private_groups
from gmb.version import version


@register_pollingbot_extension
class VersionExt(PollBotExt):
    """Extension for the version command"""
    def get_handlers(self: VersionExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('version', self.version),
                            HandlerPriority.NORMAL),
        ]

    @only_private_groups
    def version(self: VersionExt, bot: Bot, update: Update) -> None:
        """Sends the version message.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        msg = f"Current version: {version}"
        send_custom_reply(msg, bot, update)
