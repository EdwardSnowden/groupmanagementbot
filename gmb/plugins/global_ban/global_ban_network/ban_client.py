"""This module contains methods to ban users and add and remove them to the ban list."""

from __future__ import annotations

from typing import Dict, List, Optional, Tuple

from telegram import Bot, Message, User, Update

from gmb.api.resource import Chats
from gmb.plugins.global_ban.global_ban_subscription.schema_subscribe_bans import BanSubscription
from gmb.utils_design import usr_to_str
from gmb.custom_message.utils import send_custom_reply

from .schema_network_ban import NetworkBannedUsers
from .utils_telegram import TelegramBan
from .utils import ActionStatus


class BanList:
    """Methods to add and remove users to ban list."""
    @staticmethod
    def add_user(user: User, msg: Message, reason: Optional[str]) -> bool:
        """Adds a user to the banned-users database.

        Returns False if the user is already added, otherwise True.
        """
        if msg.reply_to_message is not None:
            reply_to_message_id = msg.reply_to_message.message_id
            replytext = msg.reply_to_message.text
        else:
            reply_to_message_id = None
            replytext = None
        chat = msg.chat
        from_user = msg.from_user
        if NetworkBannedUsers.try_get_by_id(user.id):
            # already banned
            return False
        NetworkBannedUsers.create(bannedid=user.id,
                                  bannedname=user.full_name,
                                  bannedusername=user.username,
                                  chatid=chat.id,
                                  chatname=chat.title,
                                  chattype=chat.type,
                                  fromid=from_user.id,
                                  fromname=from_user.full_name,
                                  fromusername=from_user.username,
                                  messageid=msg.message_id,
                                  messagereplyid=reply_to_message_id,
                                  reason=reason,
                                  date=msg.date.isoformat(),
                                  replytext=replytext)
        return True

    @staticmethod
    def remove_user(userid: int) -> bool:
        """Removes a user from the banned-users database.

        Returns True if the user is removed, otherwise False.
        """
        if not NetworkBannedUsers.try_get_by_id(userid):
            return False
        query = NetworkBannedUsers.delete().where(
            NetworkBannedUsers.bannedid == userid)
        query.execute()
        return True


class BanClient:
    """Class to perform bans in the network and to blacklist users."""
    @staticmethod
    def network_ban(bot: Bot, update: Update, user: User,
                    unban: bool = False):
        """Banns a user in all network groups.

        If unban is true, the user will be unbanned in all chats.
        """
        userid = user.id
        chat_ids = []
        chats_all = Chats.get_chats_configs()
        for chat in chats_all:
            if Chats.subscribed_global_ban(chat.id):
                chat_ids.append(chat.id)
        extern_chats = BanSubscription.get_all()
        for ban_sub in extern_chats:
            if ban_sub.active_subscription:
                chat_ids.append(ban_sub.id)
        for chat_id in chat_ids:
            if unban:
                TelegramBan.concrete_unban(bot, chat_id, userid)
            else:
                status = TelegramBan.concrete_ban(bot, chat_id, userid)
                if status is not ActionStatus.SUCCESSFULL:
                    pass
                    # TODO: https://git.thevillage.chat/thevillage/groupmanagementbot/-/issues/417
                    # msg = "A user was banned in the network chats. Error removing banned user: {}".format(usr_to_str(user))
                    # msg += "\n\nAdd the bot as admin or unsubscribe with /unsubscribe_bans."
                    # reply_to_message_id = None
                    # send_custom_reply(msg, bot, update, chat_id=chat_id,
                    #                   disable_web_page_preview=True,
                    #                   reply_to_message_id=reply_to_message_id)

    @staticmethod
    def ban_user(bot: Bot, update: Update,
                 user: User, msg: Message,
                 reason: Optional[str] = None,
                 unban: bool = False):
        """Bans a user in all network groups and adds the user to the ban list."""
        if unban:
            BanList.remove_user(user.id)
        else:
            BanList.add_user(user, msg, reason)
        BanClient.network_ban(bot, update, user, unban)

    @staticmethod
    def ban_users(bot: Bot, update: Update,
                  users: List[User],
                  msg: Message,
                  reason: Optional[str] = None,
                  unban: bool = False):
        """Adds all users to the ban list and then banns them in all network groups."""
        for user in users:
            BanClient.ban_user(bot, update, user, msg, reason, unban)
