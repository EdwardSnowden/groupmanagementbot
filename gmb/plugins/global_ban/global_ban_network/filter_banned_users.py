"""This module contains a filter that can be used to find out if a user that writes a
message or joins is a banned user."""

from telegram import Message
from telegram.ext import BaseFilter

from gmb.api.resource import Chats
from gmb.plugins.global_ban.global_ban_subscription.schema_subscribe_bans import BanSubscription

from .schema_network_ban import NetworkBannedUsers
from .utils_telegram import ParseMessages


class BannedUserFilter(BaseFilter):
    """Returns True if the user is banned.

    Has two modes. The only_join_messages mode, than it will only be triggered on join
    messages. And the second mode, if only_join_messages is False, it will be triggered
    on every message. It also Caches the list of banned users and updates the cache on
    every join message, so only if only_join_messages is activated. This causes that
    if only_join_messages is False, it will only use the cached banned users list.
    """

    def __init__(self, only_join_messages: bool = False):
        self.update_filter = False
        self.only_join_messages = only_join_messages

    def filter(self, update: Message) -> bool:
        if self.only_join_messages:
            if not update.new_chat_members:
                return False
        chat_id = update.chat_id
        chat_subscribed = Chats.subscribed_global_ban(chat_id)
        extern_subscribed = BanSubscription.try_get_by_id(chat_id)
        if not chat_subscribed and (not extern_subscribed or not extern_subscribed.active_subscription):
            return False
        for author in ParseMessages.get_authors_from_message(update):
            if NetworkBannedUsers.try_get_by_id(author.id) is not None:
                chat_admin = Chats.get_chat_admin(chat_id, author.id)
                if chat_admin is None:
                    return True
        return False
