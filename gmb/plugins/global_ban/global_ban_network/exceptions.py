"""This module contains all exceptions that are needed in the plugin."""


class WrongFormatException(Exception):
    """Is thrown, when the command has the wrong format.

    For example, you need to reply to another message and the user hasn't done this,
    then this exception is thrown and an error message is displayed to the user.
    """


class NotBannedException(Exception):
    """This exception is thrown, when an user can't be banned from a group.

    This happens for example if the user is the creator of the group.
    """
