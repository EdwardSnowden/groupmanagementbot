from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.plugins.network.network.schema_network import NetworkChatsConfig
from gmb.custom_message.utils import send_custom_reply, send_tidy_error
from gmb.permissions import only_groupadmins

from .schema_subscribe_bans import BanSubscription


@register_pollingbot_extension
class SubscribeBanExt(PollBotExt):
    def get_handlers(self: SubscribeBanExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('subscribe_bans', self.subscribe_bans),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('unsubscribe_bans', self.unsubscribe_bans),
                HandlerPriority.NORMAL),
        ]

    @staticmethod
    def send_error_network_chat(bot: Bot, update: Update) -> None:
        msg = "This chat is a network chat. Network chats can't subscribe the ban list, "
        msg += "because the bot bans there automatically."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_private_chat(bot: Bot, update: Update) -> None:
        msg = "This chat is a private chat. Private chats can't subscribe the ban list."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_already_subscribed(bot: Bot, update: Update) -> None:
        msg = "Your chat has already subscribed the global ban list."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_not_subscribed(bot: Bot, update: Update) -> None:
        msg = "Your chat has not subscribed the global ban list."
        send_tidy_error(msg, bot, update)

    @only_groupadmins
    def subscribe_bans(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        if chat.type == "private":
            SubscribeBanExt.send_error_private_chat(bot, update)
            return
        network = NetworkChatsConfig.try_get_by_id(chat.id)
        if network:
            SubscribeBanExt.send_error_network_chat(bot, update)
            return
        ban_sub = BanSubscription.try_get_by_id(chat.id)
        if ban_sub and ban_sub.active_subscription:
            SubscribeBanExt.send_error_already_subscribed(bot, update)
            return
        if not ban_sub:
            BanSubscription.create(id=chat.id, active_subscription=True)
        else:
            ban_sub.active_subscription = True
            ban_sub.save()
        msg = "Successfully subscribed global ban list. Now every ban in the network"
        msg += " will also be executed in your chat."
        send_custom_reply(msg, bot, update)

    @only_groupadmins
    def unsubscribe_bans(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        ban_sub = BanSubscription.try_get_by_id(chat.id)
        if not ban_sub or not ban_sub.active_subscription:
            SubscribeBanExt.send_error_not_subscribed(bot, update)
            return
        ban_sub.active_subscription = False
        ban_sub.save()
        msg = "Successfully unsubscribed global ban list. Now the bot won't"
        msg += " ban users in your chat anymore."
        send_custom_reply(msg, bot, update)
