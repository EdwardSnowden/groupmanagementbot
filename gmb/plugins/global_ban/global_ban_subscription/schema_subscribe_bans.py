from peewee import BigIntegerField, BooleanField

from gmb.api.database import BaseModel, register_system_config


@register_system_config
class BanSubscription(BaseModel):
    id = BigIntegerField(primary_key=True, null=False)
    active_subscription = BooleanField(null=True)
