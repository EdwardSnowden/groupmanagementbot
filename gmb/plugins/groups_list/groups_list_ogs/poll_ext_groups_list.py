from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.custom_message.utils import send_custom_reply, send_tidy_reply
from gmb.permissions import only_botadmins
from gmb.plugins.partner.ogs.schema_ogs import OGChat, OGChatsConfig
from gmb.plugins.global_ban.global_ban_subscription.schema_subscribe_bans import BanSubscription
from gmb.silent_mode import silent_and_not_private
from gmb.utils_design import list_char


@register_pollingbot_extension
class OGsListExt(PollBotExt):
    """Extension to list our ogs.

    This extension adds a command to list our chats.
    """
    def get_handlers(self: OGsListExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(CommandHandler('ogs', self.ogs),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('ogs_config', self.ogs_config),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __get_line(chat: OGChat) -> str:
        if chat.invite_link:
            out = '<a href="{}">{}</a> (#{})'
            return out.format(chat.invite_link, chat.title, chat.name)
        return '{} (#{}) (missing permissions)'.format(chat.title, chat.name)

    def ogs(self: OGsListExt, bot: Bot, update: Update) -> None:
        """Sends a list of all og chats.

        Sends a message with a list of all og chats that are in the chats config.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = OGChat.get_all()
        chats = sorted(chats, key=lambda chat: chat.name)
        chats = sorted(chats, key=lambda chat: chat.prio)
        if not chats:
            if silent_and_not_private(update):
                return
            text = "No chats added."
        else:
            text = "OG Chats:"
            current_chat = None
            # sort list by prio
            for chat in chats:
                tmp = '\n{}'.format(list_char())
                tmp += ' {}'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(OGsListExt.__get_line(chat))
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n{}'
                text += tmp.format(OGsListExt.__get_line(current_chat))
        send_tidy_reply(text, bot, update, disable_web_page_preview=True)

    @only_botadmins
    def ogs_config(self: OGsListExt, bot: Bot,
                        update: Update) -> None:
        """Sends a list of all og configs.

        Sends a message with a list of all og configs.

        Args:
            bot: The bot that sends the message.
            update: The update on that the command was triggered.
        """
        chats = OGChatsConfig.get_all()
        if not chats:
            text = "No chats added. Add them with the /add_og command."
        else:
            text = "OGs Config (id / prio / subscribed bans):"
            current_chat = None
            # sort list by prio
            for chat in chats:
                sub = BanSubscription.try_get_by_id(chat.id)
                if sub is None or not sub.active_subscription:
                    sub_add = "❌"
                else:
                    sub_add = "✅"
                tmp = '\n{}'.format(list_char())
                tmp += ' #{name} (<code>{id}</code> / {prio} / {sub_add})'
                if chat.id == update.message.chat.id:
                    current_chat = chat
                text += tmp.format(name=chat.name, id=chat.id, prio=chat.prio,
                                   sub_add=sub_add)
            if current_chat is not None:
                tmp = '\n\nAktueller Chat:\n'
                tmp += '#{name} (<code>{id}</code>)'
                text += tmp.format(name=current_chat.name,
                                   id=current_chat.id)
        send_custom_reply(text, bot, update, disable_web_page_preview=True)
