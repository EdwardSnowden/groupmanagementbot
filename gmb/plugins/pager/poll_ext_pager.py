from __future__ import annotations

from dataclasses import dataclass
from typing import List

from telegram import (Bot, CallbackQuery, Chat, InlineKeyboardButton,
                      InlineKeyboardMarkup, Message, Update)
from telegram.ext import CallbackQueryHandler, CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.pager.pager import Pager


@register_pollingbot_extension
class PagerExt(PollBotExt):
    """Extension for small commands that just sends a single message."""
    def get_handlers(self: PagerExt) -> List[PriorityHandler]:
        """Returns a list of Handlers that are added to the bot."""
        return [
            PriorityHandler(
                CallbackQueryHandler(self.current_page,
                                     pattern='^pager;([0-9]+);current_page$'),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CallbackQueryHandler(self.next_page,
                                     pattern='^pager;([0-9]+);next_page$'),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CallbackQueryHandler(self.prev_page,
                                     pattern='^pager;([0-9]+);prev_page$'),
                HandlerPriority.NORMAL),
        ]

    @staticmethod
    def current_page(_bot: Bot, update: Update):
        """Updates the pager to the current page."""
        pager = Pager.get_pager_by_update(update)
        pager.current_page(update)

    @staticmethod
    def next_page(_bot: Bot, update: Update):
        """Updates the pager to the next page."""
        pager = Pager.get_pager_by_update(update)
        pager.next_page(update)

    @staticmethod
    def prev_page(_bot: Bot, update: Update):
        """Updates the pager to the previous page."""
        pager = Pager.get_pager_by_update(update)
        pager.prev_page(update)
