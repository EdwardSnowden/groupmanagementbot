from __future__ import annotations

from typing import Callable, Type, Union

from telegram import Bot, Chat

from gmb.api.datastructs import (AbstractChat, AbstractChatsConfig,
                                 create_chat_factory)
from gmb.api.resource import ChatType, ResourceExt, register_resource_extension

from .schema_ogs import (OGChat, OGChatAdmin, OGChatsConfig,
                         OGUser)


@register_resource_extension
class OGResourceExt(ResourceExt):
    @staticmethod
    def get_chats_config_class() -> Type[OGChatsConfig]:
        return OGChatsConfig

    @staticmethod
    def get_chat_class() -> Type[OGChat]:
        return OGChat

    @staticmethod
    def get_chat_admin_class() -> Type[OGChatAdmin]:
        return OGChatAdmin

    @staticmethod
    def get_user_class() -> Type[OGUser]:
        return OGUser

    @staticmethod
    def get_chat_type() -> ChatType:
        return ChatType.EXTERN

    @staticmethod
    def factory(bot: Bot, resource_ext: Type[ResourceExt],
                chats_config: AbstractChatsConfig,
                output_dir: str) -> AbstractChat:
        chat_fac = create_chat_factory(OGResourceExt.get_name,
                                       OGResourceExt.get_fullname)
        return chat_fac(bot, resource_ext, chats_config, output_dir)

    @staticmethod
    def get_name(_res: Chat, chats_config: AbstractChatsConfig) -> str:
        return chats_config.name

    @staticmethod
    def get_fullname(res: Chat, _chats_config: AbstractChatsConfig) -> str:
        return res["title"]

    @staticmethod
    def subscribed_global_ban(chat_config: OGChatsConfig) -> bool:
        return False
