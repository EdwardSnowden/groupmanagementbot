from peewee import BooleanField

from gmb.api.database import (register_non_persistent,
                              register_non_persistent_with_dependencies,
                              register_system_config)
from gmb.api.datastructs import (AbstractChat, AbstractChatAdmin,
                                 AbstractChatsConfig, AbstractUser,
                                 chat_admin_class)


@register_system_config
class OGChatsConfig(AbstractChatsConfig):
    pass


@register_non_persistent
class OGChat(AbstractChat):
    pass


@register_non_persistent
class OGUser(AbstractUser):
    pass


@register_non_persistent_with_dependencies(OGChat, OGUser)
@chat_admin_class(OGUser, OGChat)
class OGChatAdmin(AbstractChatAdmin):
    pass
