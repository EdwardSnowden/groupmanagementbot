from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.datastructs import InfoFetchConfig
from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.api.resource import clean_all, fetch_all
from gmb.custom_message.utils import send_custom_reply
from gmb.permissions import only_botadmins

from .schema_ogs import OGChatsConfig


@register_pollingbot_extension
class OGConfigExt(PollBotExt):
    def get_handlers(self: OGConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(CommandHandler('add_og', self.add_og),
                            HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('remove_og', self.remove_og),
                HandlerPriority.NORMAL)
        ]

    @staticmethod
    def __reply_error_add_og(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/add_og chatid tag prio\n\n"
        msg += "chatid needs to be an integer, tag a string (with only letters"
        msg += " and numbers) and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __reply_error_remove_og(bot, update):
        msg = "Wrong format. Please use this format:\n"
        msg += "/remove_og tag\n\n"
        msg += "chatid needs to be an integer, tag a string and prio is an optional integer."
        send_custom_reply(msg, bot, update)

    @staticmethod
    def __check_unique(chatid: int, name: str) -> bool:
        """Returns True if id and name are not already used."""
        query = OGChatsConfig.select().where(
            OGChatsConfig.name == name)
        for og in query:
            return False
        query = OGChatsConfig.select().where(
            OGChatsConfig.id == chatid)
        for og in query:
            return False
        return True

    @only_botadmins
    def add_og(self: OGConfigExt, bot: Bot, update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) > 4 or len(text_fields) < 3:
            OGConfigExt.__reply_error_add_og(bot, update)
            return
        try:
            chatid = int(text_fields[1])
            name = text_fields[2]
            name = "".join(name.split("\n"))
            # check unique of name and id
            if not OGConfigExt.__check_unique(chatid, name):
                msg = "Chatid or tag already added.\n"
                msg += "Use /ogs_config to see all og configs."
                send_custom_reply(msg, bot, update)
                return
            if len(text_fields) > 3:
                prio = int(text_fields[3])
            else:
                prio = InfoFetchConfig.get_entry().prio_standard
        except ValueError:
            # values are in wrong order
            OGConfigExt.__reply_error_add_og(bot, update)
            return
        OGChatsConfig.create(id=chatid, name=name, prio=prio)
        msg = "Added og with tag {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        fetch_all(bot)

    @only_botadmins
    def remove_og(self: OGConfigExt, bot: Bot,
                  update: Update) -> None:
        text_fields = update.message.text.split(" ")
        if len(text_fields) != 2:
            OGConfigExt.__reply_error_remove_og(bot, update)
            return
        name = text_fields[1]
        ogs = OGChatsConfig.select().where(
            OGChatsConfig.name == name)
        og = None
        for _og in ogs:
            og = _og
        if og is None:
            # gibts nicht, fehler
            msg = "There is no og with this tag {tag}.\n"
            msg += "Use /ogs_config to see all og configs."
            msg = msg.format(tag=name)
            send_custom_reply(msg, bot, update)
            return
        name = og.name
        chatid = og.id
        prio = og.prio
        OGChatsConfig.delete().where(
            OGChatsConfig.name == name).execute()
        msg = "Removed og with tag {name} ({id}) and priority {prio}."
        msg = msg.format(name=name, id=chatid, prio=prio)
        send_custom_reply(msg, bot, update)
        clean_all(bot)
