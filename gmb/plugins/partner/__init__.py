from gmb.utils_plugin import PluginLoader


def import_plugins():
    """Imports all plugins from the plugin folder."""
    __plugin_loader = PluginLoader(__file__)
    __plugin_loader.import_plugins()
