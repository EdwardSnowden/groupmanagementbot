from __future__ import annotations

from typing import List

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.custom_message.utils import send_custom_reply
from gmb.permissions import only_botadmins
from gmb.telegram_util import reset_config_member, set_config_member

from .schema_leave_chat_config import LeaveChatConfig
from ...pollingbot.autoconfig.peewee_mapper import AutoConfigEditor
from ...pollingbot.util import PollBotExtMerge


@register_pollingbot_extension
class LeaveChatAutoConfigExt(PollBotExtMerge):
    def extensions(self):
        return [
            AutoConfigEditor(LeaveChatConfig, "leave_message", "Leave message"),
            AutoConfigEditor(LeaveChatConfig, "leave_button_1_text", "Leave button 1 text"),
            AutoConfigEditor(LeaveChatConfig, "leave_button_1_link", "Leave button 1 link"),
            AutoConfigEditor(LeaveChatConfig, "leave_button_2_text", "Leave button 2 text"),
            AutoConfigEditor(LeaveChatConfig, "leave_button_2_link", "Leave button 2 link"),
            AutoConfigEditor(LeaveChatConfig, "leave_unknown_chats", "Leave chats", "leave_chats"),
        ]
