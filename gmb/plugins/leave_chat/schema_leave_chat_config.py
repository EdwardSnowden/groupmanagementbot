"""Module that contains all configuration for the LeaveChatConfig class."""

from peewee import BooleanField, CharField, TextField

from gmb.api.database import BaseModel, register_plugin_config


@register_plugin_config
class LeaveChatConfig(BaseModel):
    """All attributes that is needed by LeaveChatConfig.

    This class saves all configuration that is needed by the LeaveChatConfig
    class in a database.
    """
    leave_unknown_chats = BooleanField(null=False, default=True)
    leave_message = TextField(
        null=False,
        default=
        'Diese Gruppe gehört nicht zum <a href="https://t.me/TheVillageChats">TheVillage Chats</a> Netzwerk.\n\n<code>{chat_name} ({chat_id})</code>'
    )
    leave_button_1_text = CharField(null=False, default="Chat")
    leave_button_1_link = CharField(null=False,
                                    default="https://t.me/TheVillageChat")
    leave_button_2_text = CharField(null=False, default="Webseite")
    leave_button_2_link = CharField(null=False,
                                    default="https://thevillage.chat")
