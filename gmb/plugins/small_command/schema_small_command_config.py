from peewee import TextField

from gmb.api.database import BaseModel, register_plugin_config


@register_plugin_config
class SmallCommandConfig(BaseModel):
    start_message = TextField(null=False, default="Hallo :)")
    license_message = TextField(
        null=False,
        default=
        "Welcome!\nThis bot is a program which is available under the <b>GNU AGPLv3</b> license at https://git.thevillage.chat/thevillage/groupmanagementbot."
    )
    dsgvo_message = TextField(
        null=False,
        default=
        "Dieser Bot speichert <b>keine persönlichen Daten</b> über normale User wie dich. Jedoch speichert er einige <b>Informationen über unsere Admins</b> ab. Diese wären der <b>Username</b>, der <b>Vorname</b> und ob ein Admin ein <b>Bot ist</b>. Dies wird benötigt um die Admins auf unserer Webseite zu verlinken."
    )
