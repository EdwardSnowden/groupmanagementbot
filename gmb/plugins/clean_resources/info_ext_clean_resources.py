from __future__ import annotations

from telegram import Bot

from gmb.api.infofetch import (InfoFetchExt,
                               register_infofetch_extension)
from gmb.api.resource import clean_all


@register_infofetch_extension
class CleanResourcesExtension(InfoFetchExt):
    @staticmethod
    def run(bot: Bot):
        clean_all(bot)
