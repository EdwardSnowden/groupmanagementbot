from peewee import TextField

from gmb.api.database import BaseModel, register_plugin_config


@register_plugin_config
class HelpMessagesConfig(BaseModel):
    pre_help_message = TextField(
        null=False,
        default=
        'Dieser Bot ist Teil des <a href="t.me/thevillagechats">TheVillage Chat</a> Netzwerkes. Er ist nur in TheVillage Gruppen und sammelt Informationen für unsere <a href="https://thevillage.chat">Webseite</a>\n\nZum einfachereren Verlinken der Chats schickt der Bot die Invite Links der Gruppen, wenn man den Hashtag derselbigen schreibt (z.B.: #dorfzentrum)\n\nBefehle:'
    )
    past_help_message = TextField(
        null=False,
        default=
        '\n\nWeitere Hilfen für Administratoren gibt es in /help_botadmin, /help_admin, /help_groupadmin und /help_superadmin'
    )
    pre_help_message_botadmin = TextField(
        null=False,
        default=
        'BotAdmins sind Administratoren mit den Sonderrechten um den Bot zu konfigurieren. Damit ein NetworkAdmin ein BotAdmin wird, muss man ihn mit /promote zu einem BotAdmin promoten.\n\nBotAdmin Befehle:'
    )
    past_help_message_botadmin = TextField(null=False, default='')
    pre_help_message_admin = TextField(
        null=False,
        default=
        'NetworkAdmins sind in diesem Bot nicht die Admins in den Gruppen (die werden hier Groupadmins genannt), sondern Administratoren des Bot. Dabei gibt es zwei Arten von Admins, einmal die NetworkAdmins und einmal die BotAdmins. BotAdmins können den Bot konfigurieren, während NetworkAdmins den Bot bedienen können. Diese NetworkAdmins müssen von einem Superadmin oder BotAdmin mit dem Befehl /add_admin oder /promote befördert werden und können dann die hier aufgelisteten Befehle nutzen.\n\nAdmin Befehle:'
    )
    past_help_message_admin = TextField(null=False, default='')
    pre_help_message_superadmin = TextField(
        null=False,
        default='Der Superadmin ist der Admin vom Bot.\n\nSuperAdmin Befehle:')
    past_help_message_superadmin = TextField(null=False, default='')
    pre_help_message_groupadmin = TextField(
        null=False,
        default=
        'Als Groupadmin zählen alle Admins die in Gruppen die Admin-Rechte haben.\n\nGroupadmin Befehle:'
    )
    past_help_message_groupadmin = TextField(null=False, default='')
