from __future__ import annotations

from telegram import Bot

from gmb.api.infofetch import (InfoFetchExt, register_infofetch_extension)
from gmb.api.resource import fetch_all


@register_infofetch_extension
class GetResourcesExtension(InfoFetchExt):
    @staticmethod
    def run(bot: Bot):
        fetch_all(bot)
