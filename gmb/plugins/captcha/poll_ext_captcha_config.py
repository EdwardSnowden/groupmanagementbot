from __future__ import annotations

from typing import List, Optional

from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.api.pollingbot import (HandlerPriority, PollBotExt, PriorityHandler,
                                register_pollingbot_extension)
from gmb.plugins.network.network.schema_network import NetworkChatsConfig
from gmb.custom_message.utils import send_custom_reply, send_tidy_error
from gmb.permissions import only_groupadmins, only_botadmins

from .schema_captcha_config import CaptchaAdditionalChatsConfig, CaptchaConfig
from ...pollingbot.autoconfig.peewee_mapper import AutoConfigEditor
from ...pollingbot.util import PollBotExtMerge


@register_pollingbot_extension
class CaptchaConfigAutoExt(PollBotExtMerge):
    def extensions(self):
        return [
            AutoConfigEditor(CaptchaConfig, "captcha_button", "Captcha button"),
            AutoConfigEditor(CaptchaConfig, "captcha_text", "Captcha text"),
            AutoConfigEditor(CaptchaConfig, "captcha_time", "Captcha time")
        ]


@register_pollingbot_extension
class CaptchaConfigExt(PollBotExt):
    def get_handlers(self: CaptchaConfigExt) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('enable_captcha', self.enable_captcha),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('disable_captcha', self.disable_captcha),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('enable_network_captcha', self.enable_network_captcha),
                HandlerPriority.NORMAL),
            PriorityHandler(
                CommandHandler('disable_network_captcha', self.disable_network_captcha),
                HandlerPriority.NORMAL),
        ]

    @staticmethod
    def send_error_network_chat(bot: Bot, update: Update) -> None:
        msg = "This chat is a network chat. To enable or disable captchas here "
        msg += "you need to use the <code>/enable_network_captcha</code> "
        msg += "and <code>/disable_network_captcha</code> commands."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_private_chat(bot: Bot, update: Update) -> None:
        msg = "This chat is a private chat. Captchas don't work in private chats."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_already_enabled(bot: Bot, update: Update) -> None:
        msg = "Your chat has already enabled captchas."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_not_enabled(bot: Bot, update: Update) -> None:
        msg = "Your chat has not enabled the captchas."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_already_enabled_network(bot: Bot, update: Update) -> None:
        msg = "Captchas are already enabled in the network."
        send_tidy_error(msg, bot, update)

    @staticmethod
    def send_error_already_disabled_network(bot: Bot, update: Update) -> None:
        msg = "Captchas are already disabled in the network."
        send_tidy_error(msg, bot, update)

    @only_groupadmins
    def enable_captcha(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        if chat.type == "private":
            CaptchaConfigExt.send_error_private_chat(bot, update)
            return
        network = NetworkChatsConfig.try_get_by_id(chat.id)
        if network:
            CaptchaConfigExt.send_error_network_chat(bot, update)
            return
        cap_chat_conf: Optional[CaptchaAdditionalChatsConfig] = CaptchaAdditionalChatsConfig.try_get_by_id(chat.id)
        if cap_chat_conf and cap_chat_conf.enabled_captcha:
            CaptchaConfigExt.send_error_already_enabled(bot, update)
            return
        if not cap_chat_conf:
            CaptchaAdditionalChatsConfig.create(chat_id=chat.id, enabled_captcha=True)
        else:
            cap_chat_conf.enabled_captcha = True
            cap_chat_conf.save()
        msg = "Successfully enabled captchas. Now new users that joines"
        msg += " needs to prove they are humans."
        send_custom_reply(msg, bot, update)

    @only_groupadmins
    def disable_captcha(self, bot: Bot, update: Update) -> None:
        chat = update.effective_chat
        cap_chat_conf: Optional[CaptchaAdditionalChatsConfig] = CaptchaAdditionalChatsConfig.try_get_by_id(chat.id)
        if not cap_chat_conf or not cap_chat_conf.enabled_captcha:
            CaptchaConfigExt.send_error_not_enabled(bot, update)
            return
        cap_chat_conf.enabled_captcha = False
        cap_chat_conf.save()
        msg = "Successfully disabled captchas."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def enable_network_captcha(self, bot: Bot, update: Update) -> None:
        cap_conf: CaptchaConfig = CaptchaConfig.get_entry()
        if cap_conf.enabled:
            CaptchaConfigExt.send_error_already_enabled_network(bot, update)
            return
        cap_conf.enabled = True
        cap_conf.save()
        msg = "Successfully enabled captchas in the network. Now new users that joines"
        msg += " needs to prove they are humans."
        send_custom_reply(msg, bot, update)

    @only_botadmins
    def disable_network_captcha(self, bot: Bot, update: Update) -> None:
        cap_conf: CaptchaConfig = CaptchaConfig.get_entry()
        if not cap_conf.enabled:
            CaptchaConfigExt.send_error_already_disabled_network(bot, update)
            return
        cap_conf.enabled = False
        cap_conf.save()
        msg = "Successfully disabled captchas in the network."
        send_custom_reply(msg, bot, update)
