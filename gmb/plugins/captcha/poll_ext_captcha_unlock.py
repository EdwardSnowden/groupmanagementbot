from typing import List

from telegram import Bot, Update, CallbackQuery
from telegram.ext import CallbackQueryHandler

from gmb.permissions import has_role, Role
from gmb.plugins.captcha.schema_captcha_config import UserCaptcha
from gmb.pollingbot.extension import PollBotExt, PriorityHandler, HandlerPriority
from gmb.pollingbot.extensions import register_pollingbot_extension


@register_pollingbot_extension
class CaptchaUnlockExt(PollBotExt):
    def get_handlers(self) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CallbackQueryHandler(self.unlock_captcha,
                                     pattern='^capt;([0-9]+)$'),
                HandlerPriority.NORMAL),
        ]

    def unlock_captcha(self, bot: Bot, update: Update) -> None:
        chat_id = update.effective_chat.id
        triggered_user_id = update.effective_user.id
        query: CallbackQuery = update.callback_query
        cb_data = query.data
        related_user_id = int(cb_data.split(";")[1])
        if triggered_user_id != related_user_id:
            # if user is admin, solve the captcha
            if not has_role(update.effective_user , update.effective_chat, Role.GROUP_ADMIN):
                return
        user_id = related_user_id
        captcha = UserCaptcha.get_or_none(UserCaptcha.user_id == user_id, UserCaptcha.chat_id == chat_id)
        if captcha is not None:
            captcha.captcha_solved = True
            captcha.save()
            captcha.delete_instance()
            update.effective_message.delete()
