from peewee import TextField, BooleanField, IntegerField, BigIntegerField, DateTimeField, DoesNotExist, CompositeKey

from gmb.api.database import BaseModel, register_plugin_config
from gmb.database.error_handler import handle_errors


@register_plugin_config
class CaptchaConfig(BaseModel):
    """General global configuration of captchas.

    enabled: if this is true, the captchas are enabled in the network
    captcha_text: this text is send if a user joines a chat
    captcha_button: this is the text of the captcha button
    captcha_time: time in seconds to press the button
    """
    enabled = BooleanField(null=False, default=False)
    captcha_text = TextField(null=False, default='Hallo. Um schreiben zu können musst du innerhalb von {captcha_time} Sekunden auf den Knopf drücken, alternativ wirst du gebannt!')
    captcha_button = TextField(null=False, default='Ich bin kein Bot')
    captcha_time = IntegerField(null=False, default=60)


@register_plugin_config
class CaptchaAdditionalChatsConfig(BaseModel):
    """Captcha configuration for partner chats.

    chat_id: the id of the partner chat
    enabled_captcha: if this is true, captchas are enabled in this chat
    """
    chat_id = BigIntegerField(primary_key=True, null=False)
    enabled_captcha = BooleanField(null=False, default=False)


@register_plugin_config
class UserCaptcha(BaseModel):
    """If a user joines a group, he will be restricted and his information is stored here."""
    user_id = BigIntegerField(null=False)
    chat_id = BigIntegerField(null=False)
    captcha_solved = BooleanField(null=False, default=False)
    joined_timestamp = IntegerField(null=False)

    class Meta:
        primary_key = CompositeKey("user_id", "chat_id")

    @classmethod
    def _get_or_create_user_captcha(cls, user_id, chat_id, **kwargs):
        try:
            item = super(BaseModel, cls).get(user_id=user_id, chat_id=chat_id)
        except DoesNotExist:
            item = super(BaseModel, cls).create(user_id=user_id, chat_id=chat_id, **kwargs)
        return item

    @classmethod
    @handle_errors
    def get_or_create_user_captcha(cls, user_id, chat_id, **kwargs):
        return cls._get_or_create_user_captcha(user_id, chat_id, **kwargs)
