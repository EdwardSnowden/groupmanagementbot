"""This module contains a filter that can be used to find out if a user that writes a
message or joins is a banned user."""

from telegram import Message, Bot, Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.error import BadRequest
from telegram.ext import BaseFilter, MessageHandler
from typing import List, Callable
import time

from .schema_captcha_config import CaptchaConfig, CaptchaAdditionalChatsConfig, UserCaptcha
from ..network.network.schema_network import NetworkChatsConfig
from ...custom_message.custom_message import CustomMessage
from ...permissions import Role, has_role
from ...pollingbot.extension import PollBotExt, PriorityHandler, HandlerPriority
from ...pollingbot.extensions import register_pollingbot_extension
from ...telegram_util import DeleteCallbackReturn


class CaptchaFilter(BaseFilter):
    """Triggers on join messages.

    On network chats: returns the boolean of the network-chat config
    On others, if there is no special config: return false
    On others, if there is a special config: return the boolean of the special config
    """

    def __init__(self):
        pass

    def filter(self, update: Message) -> bool:
        if not update.new_chat_members:
            return False
        chat_id = update.chat_id
        # network
        if has_role(update.from_user, update.chat, Role.GROUP_ADMIN):
            return False
        network = NetworkChatsConfig.try_get_by_id(chat_id)
        if network is not None:
            return CaptchaConfig.get_entry().enabled
        # no network
        chats_config = CaptchaAdditionalChatsConfig.try_get_by_id(chat_id)
        if chats_config is None:
            # captcha needs to be activated explicitly
            return False
        return chats_config.enabled_captcha


@register_pollingbot_extension
class CaptchaExt(PollBotExt):
    def get_handlers(self) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                MessageHandler(CaptchaFilter(), self.users_joined),
                HandlerPriority.NORMAL, 2),
        ]

    @staticmethod
    def __create_callback(bot: Bot, user_id: int, chat_id: int) -> Callable[[], DeleteCallbackReturn]:
        """creates a function that is executed after an amount of time and that kicks the user if he doesn't solve the
        captcha."""
        def intern():
            captcha = UserCaptcha.get_or_none(UserCaptcha.user_id == user_id, UserCaptcha.chat_id == chat_id)
            if captcha and not captcha.captcha_solved:
                # user has not solved the captcha, so kick the user
                # unban kicks the user
                try:
                    bot.unban_chat_member(chat_id, user_id)
                except BadRequest:
                    # chat owners joining there own chat raises this exception
                    pass
                captcha.delete_instance()
                return DeleteCallbackReturn.DELETE_ALL
            return DeleteCallbackReturn.DELETE_WITHOUT_REPLY_TO
        return intern

    @staticmethod
    def __send_captcha(bot: Bot, update: Update, user_id: int, chat_id: int):
        captch_time = CaptchaConfig.get_entry().captcha_time
        text = CaptchaConfig.get_entry().captcha_text
        text = text.replace("{captcha_time}", str(captch_time))
        button_txt = CaptchaConfig.get_entry().captcha_button
        call_data = f"capt;{user_id}"
        buttons = [[InlineKeyboardButton(button_txt, callback_data=call_data)]]
        markup = InlineKeyboardMarkup(buttons)
        extra = {
            "reply_markup": markup,
        }
        msg = CustomMessage(text, bot, update, **extra)
        msg.timer.activate_tidy_special(captch_time)
        callback = CaptchaExt.__create_callback(bot, user_id, chat_id)
        msg.timer.add_callback(callback)
        send = msg.send()
        return send

    def users_joined(self, bot: Bot, update: Update) -> None:
        """Is triggered when one or more users join."""
        chat_id = update.effective_chat.id
        time.sleep(1)
        for user in update.message.new_chat_members:
            if has_role(user, update.effective_chat, Role.GROUP_ADMIN):
                print("Not sending a captcha, because user is an admin.")
                return
            user_id = user.id
            timestamp = time.time()
            # in some chats the user cant see the history, so you need to send it a second time
            UserCaptcha.get_or_create_user_captcha(user_id=user_id, chat_id=chat_id,
                                                   captcha_solved=False, joined_timestamp=timestamp)
            # send captcha
            CaptchaExt.__send_captcha(bot, update, user_id, chat_id)
