"""Module that contains all splitting methods for pages."""

import time
from typing import List, Union

from .parser import HTMLParser, IParserItem


class IPageSplittingMethod:
    """Interface for methods that splits a text into pages."""
    def gen_paged_list(self, text: str) -> List[str]:
        """Returns a list with the pages of the text."""
        raise NotImplementedError()


class PageSplitByCharLength(IPageSplittingMethod):
    """Splits a text into pages by an amount of chars per page."""
    def __init__(self, chars_per_page: int):
        """Creates a PageSplitByCharLength."""
        self.chars_per_page = chars_per_page

    def gen_amount_pages_by_length(self, len_text: int) -> int:
        """Returns the amount of pages for a length of text."""
        pages_f = len_text / self.chars_per_page
        pages = int(pages_f)
        if pages < pages_f:
            pages += 1
        return pages

    def gen_paged_list(self, text: str) -> List[str]:
        """Splits a message into pages of chars per page."""
        len_text = len(text)
        am_pag = self.gen_amount_pages_by_length(len_text)
        msgs = []
        for page_index in range(am_pag):
            first_index = self.chars_per_page * page_index
            last_index = self.chars_per_page * (page_index + 1)
            msgs.append(text[first_index:last_index])
        return msgs


class PageSplitByCharLengthHTML(IPageSplittingMethod):
    """Splits a text into pages by an amount of chars per page."""
    def __init__(self, chars_per_page: int):
        """Creates a PageSplitByCharLengthHTML."""
        self.chars_per_page = chars_per_page

    def gen_amount_pages_by_length(self, len_text: int) -> int:
        """Returns the amount of pages for a length of text."""
        pages_f = len_text / self.chars_per_page
        pages = int(pages_f)
        if pages < pages_f:
            pages += 1
        return pages

    def gen_paged_list(self, text: str) -> List[str]:
        """Splits a message into pages of chars per page."""
        item: IParserItem = HTMLParser.parse(text)
        len_text = len(item)
        am_pag = self.gen_amount_pages_by_length(len_text)
        msgs = []
        for page_index in range(am_pag):
            first_index = self.chars_per_page * page_index
            last_index = self.chars_per_page * (page_index + 1)
            msgs.append(str(item[first_index:last_index]))
        return msgs


def indexof(string: str, chars: Union[List[str], str]) -> int:
    """Returns the highest index in string where one of the given substrings is
    found."""
    if isinstance(chars, str):
        return string.rfind(chars)
    if len(chars) <= 0:
        return -1
    pos = []
    for char in chars:
        pos.append(string.rfind(char))
    pos.sort()
    return pos.pop()


class PageSplitAlgo1(IPageSplittingMethod):
    """A page split algorithm that splits on new lines, sentences and words."""
    def __init__(self, chars_per_page: int):
        """Creates a PageSplitAlgo1."""
        self.chars_per_page = chars_per_page

    def gen_paged_list(self, text: str) -> List[str]:
        """Generates the paged lists."""
        item: IParserItem = HTMLParser.parse(text)
        len_item = len(item)
        first_pos = 0
        # next_char = item[last_pos_tmp:last_pos_tmp + 1]
        # if str(next_char) == " ":
        #     # word
        #     pass
        msgs = []
        while first_pos < len_item:
            last_pos_max = first_pos + self.chars_per_page
            page = item[first_pos:last_pos_max]
            if len_item < last_pos_max:
                # last page
                msgs.append(str(item[first_pos:last_pos_max]))
                break
            last_pos = first_pos + indexof(page.without_tags(), "\n")
            if last_pos > first_pos and last_pos_max - last_pos < self.chars_per_page * 0.2:
                msgs.append(str(item[first_pos:last_pos + 1]))
                first_pos = last_pos + 1
                continue
            last_pos = first_pos + indexof(page.without_tags(),
                                           [". ", "! ", "? "])
            if last_pos > first_pos and last_pos_max - last_pos < self.chars_per_page * 0.2:
                msgs.append(str(item[first_pos:last_pos + 1]))
                first_pos = last_pos + 2  # +1 and +2, because skip the space
                continue
            last_pos = first_pos + indexof(page.without_tags(), " ")
            if last_pos > first_pos and last_pos_max - last_pos < self.chars_per_page * 0.2:
                msgs.append(str(item[first_pos:last_pos]))
                first_pos = last_pos + 1
                continue
            msgs.append(str(item[first_pos:last_pos_max - 1]) + "-")
            first_pos = last_pos_max - 1
        return msgs
