"""Unittests for the module page_splitting_methods."""

import unittest

from ddt import data, ddt, unpack

from .page_splitting_methods import (IPageSplittingMethod, PageSplitAlgo1,
                                     PageSplitByCharLength,
                                     PageSplitByCharLengthHTML, indexof)


@ddt
class TestIPageSplittingMethod(unittest.TestCase):
    """Unittests for the interface IPageSplittingMethod."""
    def test_raise(self):
        """Tests if the interface raises not implemented."""
        with self.assertRaises(NotImplementedError):
            IPageSplittingMethod().gen_paged_list("egal")


@ddt
class TestPageSplitByCharLength(unittest.TestCase):
    """Unittests for the class PageSplitByCharLength."""
    @data(
        ("Hallo Welt! Hallo Welt!", ["Hallo Welt", "! Hallo We", "lt!"], 10),
        ("Lorem Ipsum dolor set amet.",
         ['Lorem', ' Ipsu', 'm dol', 'or se', 't ame', 't.'], 5),
    )
    @unpack
    def test_len(self, string, arr, amount):
        """Tests if PageSplitByCharLength works."""
        pages = PageSplitByCharLength(amount).gen_paged_list(string)
        self.assertEqual(pages, arr)
        am_pages = PageSplitByCharLength(amount).gen_amount_pages_by_length(
            len(string))
        self.assertEqual(am_pages, len(pages))


@ddt
class TestPageSplitByCharLengthHTML(unittest.TestCase):
    """Unittests for the class PageSplitByCharLengthHTML."""
    @data(
        ("Hallo Welt! Hallo Welt!", ['Hallo Welt', '! Hallo We', 'lt!'], 23),
        ("Hallo <b>Welt</b>! Hallo <b>Welt</b>!",
         ['Hallo <b>Welt</b>', '! Hallo <b>We</b>', '<b>lt</b>!'], 23),
        ("Hallo <i>Welt</i>! Hallo <i>Welt</i>!",
         ['Hallo <i>Welt</i>', '! Hallo <i>We</i>', '<i>lt</i>!'], 23),
        ("Hallo <code>Welt</code>! Hallo <code>Welt</code>!", [
            'Hallo <code>Welt</code>', '! Hallo <code>We</code>',
            '<code>lt</code>!'
        ], 23),
        ("Hallo <pre>Welt</pre>! Hallo <pre>Welt</pre>!", [
            'Hallo <pre>Welt</pre>', '! Hallo <pre>We</pre>', '<pre>lt</pre>!'
        ], 23),
        ('Hallo <a href="a.com">Welt</a>! Hallo <a href="a.com">Welt</a>!', [
            'Hallo <a href="a.com">Welt</a>', '! Hallo <a href="a.com">We</a>',
            '<a href="a.com">lt</a>!'
        ], 23),
        ("Hallo &lt; Welt! Hallo &lt; Welt!",
         ['Hallo &lt; We', 'lt! Hallo ', '&lt; Welt!'], 25),
        ("Hallo &gt; Welt! Hallo &gt; Welt!",
         ['Hallo &gt; We', 'lt! Hallo ', '&gt; Welt!'], 25),
        ("Hallo &amp; Welt! Hallo &amp; Welt!",
         ['Hallo &amp; We', 'lt! Hallo ', '&amp; Welt!'], 25),
        ("Hallo &quot; Welt! Hallo &quot; Welt!",
         ['Hallo &quot; We', 'lt! Hallo ', '&quot; Welt!'], 25),
    )
    @unpack
    def test_len_html(self, string, arr, expected_len):
        """Tests if PageSplitByCharLengthHTML works for all html tags."""
        pages = PageSplitByCharLengthHTML(10).gen_paged_list(string)
        self.assertEqual(pages, arr)
        am_pages = PageSplitByCharLength(10).gen_amount_pages_by_length(
            expected_len)
        self.assertEqual(am_pages, len(pages))


@ddt
class TestIndexOf(unittest.TestCase):
    """Testing the index of method."""
    @data(("Hallo!", 5), ("!Hallo", 0))
    @unpack
    def test_str(self, string, pos):
        """Testing finding a singe char."""
        index = indexof(string, "!")
        self.assertEqual(index, pos)

    def test_len(self):
        """Testing empty searches."""
        index = indexof("iwas", [])
        self.assertEqual(index, -1)

    @data("Hallo!", "Hallo.", "Hallo?")
    def test_list(self, string):
        """Testing finding more than one char."""
        index = indexof(string, ["!", ".", "?"])
        self.assertEqual(index, 5)


class TestPageSplitAlgo1(unittest.TestCase):
    """Testing the split algo 1."""
    def test_special_chars(self):
        """Testing html split."""
        string = "Nam molestiae libero quo reiciendis earum nulla. Aut sunt"
        string += " exercitationem ut cumque id itaque. Omnis inventore consectetur"
        string += " reprehenderit. Et nihil fugiat at architecto est in modi. Eos vero"
        string += " pariatur voluptate nostrum. Cupiditate modi delectus et aut tempora."
        arr = [
            'Nam molestiae libero quo reiciendis earum nulla.',
            'Aut sunt exercitationem ut cumque id itaque.',
            'Omnis inventore consectetur reprehenderit.',
            'Et nihil fugiat at architecto est in modi.',
            'Eos vero pariatur voluptate nostrum. Cupiditate',
            'modi delectus et aut tempora.'
        ]
        pages = PageSplitAlgo1(50).gen_paged_list(string)
        self.assertEqual(pages, arr)

    def test_len_html(self):
        """Testing html split."""
        string = '<b>Nam molestiae </b>libero quo reiciendis earum <a href="beispiel.de">'
        string += "nulla. Aut</a> sunt exercitationem ut cumque id itaque. Omnis"
        string += " inventore consectetur reprehenderit. Et nihil <code>fugiat at"
        string += " architecto est in modi. Eos</code> vero pariatur voluptate"
        string += " <i>nostrum</i>. Cupiditate modi delectus et aut tempora."
        arr = [
            '<b>Nam molestiae </b>libero quo reiciendis earum <a href="beispiel.de">nulla.</a>',
            '<a href="beispiel.de">Aut</a> sunt exercitationem ut cumque id itaque.',
            'Omnis inventore consectetur reprehenderit.',
            'Et nihil <code>fugiat at architecto est in modi.</code>',
            '<code>Eos</code> vero pariatur voluptate <i>nostrum</i>. Cupiditate',
            'modi delectus et aut tempora.'
        ]
        pages = PageSplitAlgo1(50).gen_paged_list(string)
        self.assertEqual(pages, arr)

    def test_new_line(self):
        """Testing new line."""
        string = "Hallo.\nHallo"
        pages = PageSplitAlgo1(7).gen_paged_list(string)
        self.assertEqual(pages, ["Hallo.\n", "Hallo"])

    def test_no_split(self):
        """Testing splitting Words without any special chars."""
        string = "HalloWelt."
        pages = PageSplitAlgo1(6).gen_paged_list(string)
        self.assertEqual(pages, ["Hallo-", "Welt."])
