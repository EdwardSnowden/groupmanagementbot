"""This module contains an html parser that can parse the telegram html markup and is able
to split messages while returning correct html objects."""

from __future__ import annotations

from dataclasses import dataclass
from enum import Enum, auto
from typing import List, Optional, Tuple, Union


class ItemType(Enum):
    """Types of html tag."""
    BOLD = auto()
    ITALIC = auto()
    CODE = auto()
    PRE = auto()
    URL = auto()
    STRIKE = auto()
    UNDERLINE = auto()
    SONDERZEICHEN = auto()


@dataclass
class Sonderzeichen:
    """Dataclass to describe special characters."""
    zeichen: str
    length: int
    original: str


SONDERZEICHEN = [
    Sonderzeichen("&lt;", 1, "<"),
    Sonderzeichen("&gt;", 1, ">"),
    Sonderzeichen("&amp;", 1, "&"),
    Sonderzeichen("&quot;", 1, '"'),
]


def find_first_sonderzeichen(string: str
                             ) -> Optional[Tuple[Sonderzeichen, int]]:
    """Returns the position of the fist special character."""
    first_zeichen: Optional[Sonderzeichen] = None
    first_pos: Optional[int] = None
    for zeichen in SONDERZEICHEN:
        pos = string.find(zeichen.zeichen)
        if pos == -1:
            continue
        if first_pos is None or pos < first_pos:
            first_pos = pos
            first_zeichen = zeichen
    if first_pos is None or first_zeichen is None:
        return None
    return (first_zeichen, first_pos)


ITEM_TYPES_TAGS = {
    ItemType.BOLD: ["b", "strong"],
    ItemType.ITALIC: ["i", "em"],
    ItemType.CODE: ["code"],
    ItemType.PRE: ["pre"],
    ItemType.URL: ["a"],
    ItemType.STRIKE: ["s", "strike"],
    ItemType.UNDERLINE: ["u"],
}


class IParserItem:
    """An element of the html structure."""
    def __len__(self):
        """Returns the length of the string without tags."""
        raise NotImplementedError()

    def __str__(self):
        """Returns the string with tags."""
        raise NotImplementedError()

    def __getitem__(self, sliced: Union[int, slice]) -> IParserItem:
        """The split is applied on the string without tags and the return is an
        IParserItem with valid html containing the tags.

        Example: The string `<b>Hallo Welt</b>` splitted with `[:5]` would return
        the IParserItem `<b>Hallo</b>`"""
        raise NotImplementedError()

    def without_tags(self) -> str:
        """Returns the string without tags."""
        raise NotImplementedError()


class ParserCollection(IParserItem):
    """A collection of IParserItems."""
    def __init__(self, items: Union[List[IParserItem], ParserCollection]):
        """Creates a ParserCollection."""
        if isinstance(items, list):
            self.items = items.copy()
        elif isinstance(items, ParserCollection):
            self.items = items.items.copy()
        else:
            raise TypeError(
                "items must be List[IParserItem] or ParserCollection")

    def __len__(self) -> int:
        """Returns the length of the string without tags."""
        return sum(len(item) for item in self.items)

    def without_tags(self) -> str:
        """Returns the string without tags."""
        out = ""
        for item in self.items:
            out += item.without_tags()
        return out

    def __str__(self) -> str:
        """Returns the string with tags."""
        out = ""
        for item in self.items:
            out += str(item)
        return out

    def append(self, item: IParserItem):
        """Appends an item to the list."""
        self.items.append(item)

    # def extend(self, collection: Union[List[IParserItem], ParserCollection]):
    #     """Extends the collection."""
    #     if isinstance(collection, ParserCollection):
    #         self.items.extend(collection.items)
    #     if isinstance(collection, list):
    #         self.items.extend(collection)
    #     else:
    #         raise TypeError("collection mus be List[IParserItem] or ParserCollection")

    def __getitem__(self, sliced: Union[int, slice]) -> ParserCollection:
        """The split is applied on the string representation of the collection without
        tags and the return is an ParserCollection with valid html containing the tags."""
        if isinstance(sliced, int):
            raise NotImplementedError("Int slices are not implemented.")
        if isinstance(sliced, slice):
            start = sliced.start if sliced.start is not None else 0
            stop = sliced.stop if sliced.stop is not None else len(self)
            if start < 0 or stop < 0:
                raise NotImplementedError()
            if sliced.step is not None and sliced.step != 1:
                raise NotImplementedError()
            pos = 0
            liste = []
            started = False
            for item in self.items:
                if started:
                    liste.append(item[0:stop - pos])
                elif pos + len(item) > start:
                    liste.append(item[start - pos:stop - pos])
                    started = True
                pos += len(item)
                if pos > stop:
                    return ParserCollection(liste)
            return ParserCollection(liste)
        raise TypeError("indices must be integers or slices")


class ParserString(IParserItem):
    """A parser item that contains a string."""
    def __init__(self, string: str):
        """Creates a ParserString."""
        self.string = string

    def __len__(self):
        """Returns the length of the string."""
        return len(self.string)

    def without_tags(self) -> str:
        """Returns the string."""
        return self.string

    def __str__(self):
        """Returns the string."""
        return self.string

    def __getitem__(self, sliced: Union[int, slice]) -> ParserString:
        """The split is applied on the string."""
        return ParserString(self.string[sliced])


class ParserSonderzeichen(IParserItem):
    """A parser item that contains a special char."""
    def __init__(self, zeichen: Sonderzeichen):
        """Creaes a ParserSonderzeichen."""
        self.string = zeichen.zeichen
        self.length = zeichen.length
        self.original = zeichen.original

    def __len__(self):
        """Returns the length of the special char."""
        return self.length

    def without_tags(self) -> str:
        """Returns the original character of the special char."""
        return self.original

    def __str__(self):
        """Returns the special char in escaped form."""
        return self.string

    def __getitem__(self, sliced: Union[int, slice]) -> ParserSonderzeichen:
        """Returns the special char if it's in the range or returns an empty char."""
        if isinstance(sliced, int):
            raise NotImplementedError("Int slices are not implemented.")
        if isinstance(sliced, slice):
            if sliced.start == 0:
                if sliced.stop != 0:
                    return self
            return ParserSonderzeichen(Sonderzeichen("", 0, ""))
        raise TypeError("indices must be integers or slices")


class ParserSonderzeichenString(ParserCollection):
    """A parser item that contains a string with potential special characters."""
    def __init__(self, string: str):
        """Creates a ParserSonderzeichenString.

        During that all special chars will be detected and saved separately."""
        super().__init__([])
        while True:
            tmp = find_first_sonderzeichen(string)
            if tmp is None:
                self.append(ParserString(string))
                break
            zeichen, pos = tmp
            if pos != 0:
                pre_string = string[:pos]
                self.append(ParserString(pre_string))
            self.append(ParserSonderzeichen(zeichen))
            string = string[pos + len(zeichen.zeichen):]


class ParserTag(ParserCollection):
    """A parser item that contains a string within a tag."""
    def __init__(self, string: Union[str, ParserCollection],
                 item_type: ItemType):
        """Creates a ParserTag."""
        if isinstance(string, str):
            super().__init__(ParserSonderzeichenString(string))
        elif isinstance(string, ParserCollection):
            super().__init__(string)
        self.item_type = item_type

    def __str__(self):
        """Returns the string with tags."""
        tag = ITEM_TYPES_TAGS[self.item_type][0]
        return '<' + '{0}>{1}</{0}>'.format(tag, super().__str__())

    def __getitem__(self, sliced: Union[int, slice]) -> ParserTag:
        """The split is applied on the string representation of the collection without
        tags and the return is an ParserCollection with valid html containing the tags."""
        return ParserTag(super().__getitem__(sliced), self.item_type)


class ParserUrl(ParserCollection):
    """A parser item that contains a url."""
    def __init__(self, string: Union[str, ParserCollection], url: str):
        """Creates a ParserUrl."""
        if isinstance(string, str):
            super().__init__(ParserSonderzeichenString(string))
        elif isinstance(string, ParserCollection):
            super().__init__(string)
        self.url = url

    def __str__(self):
        """Returns the string with tags and attributes."""
        return '<a href="{}">{}</a>'.format(self.url, super().__str__())

    def __getitem__(self, sliced: Union[int, slice]) -> ParserUrl:
        """The split is applied on the string representation of the collection without
        tags and the return is an ParserCollection with valid html containing the tags."""
        return ParserUrl(super().__getitem__(sliced), self.url)


@dataclass
class HTMLElement:
    """Dataclass to describe an html element."""
    tag: str
    attribute: Optional[Tuple[str, str]]
    content: str
    start: int
    end: int


class HTMLParser:
    """A parser that can parse the telegram html markup."""
    @staticmethod
    def __parse_attribute(attribute_str: Optional[str]
                          ) -> Optional[Tuple[str, str]]:
        """Returns the name and value of the given attribute-string."""
        if attribute_str is None:
            return None
        attribute_str = attribute_str.strip()
        name, value = attribute_str.split('=', 1)
        name = name.strip()
        value = value.strip()
        if value[0] == '"':
            value = value[1:len(value) - 1]
        return (name, value)

    @staticmethod
    def __find_first_tag(string: str):
        """Returns the required positions of the first tag and the position of its closing
        tag."""
        start_open = string.find("<")
        if start_open == -1:  # error or no tags
            start_end = string.find(">")
            if start_end != -1:
                raise TypeError("invalid html")
            return None
        start_end = string.find(">", start_open)
        if start_end == -1:
            raise TypeError("invalid html")
        start_tag_tmp = string[start_open + 1:start_end].strip()
        splitted = start_tag_tmp.split(" ", 1)
        if len(splitted) > 1:
            start_tag, attribute_str = splitted
        else:
            start_tag = splitted[0]
            attribute_str = None
        attribute = HTMLParser.__parse_attribute(attribute_str)
        end_open = string.find("</{}".format(start_tag), start_end)
        if end_open == -1:
            raise TypeError("invalid html")
        end_end = string.find(">", end_open)
        if end_end == -1:
            raise TypeError("invalid html")
        content = string[start_end + 1:end_open]
        return HTMLElement(start_tag, attribute, content, start_open, end_end)

    @staticmethod
    def __create_parser_item(element: HTMLElement) -> IParserItem:
        """Creates the parser item that is related to the given html element."""
        for item_type in ITEM_TYPES_TAGS:
            if element.tag.lower() in ITEM_TYPES_TAGS[item_type]:
                # recursive items:
                # if item_type == ItemType.LI:
                #     intern_collection = HTMLParser().parse(element.content)
                #     return ParserList(intern_collection)
                if item_type == ItemType.URL:
                    attr = element.attribute
                    if attr is not None and len(
                            attr) > 0 and attr[0] == "href":
                        return ParserUrl(element.content, element.attribute[1])
                    raise TypeError(
                        "invalid html: 'a' tag needs 'href' attribute")
                return ParserTag(element.content, item_type)
        raise TypeError(f"Unknown html tag found: {element.tag.lower()}")

    @staticmethod
    def parse(html: str) -> IParserItem:
        """Parses the html."""
        items = []
        while True:
            element = HTMLParser.__find_first_tag(html)
            if element is None:
                if len(html) > 0:
                    items.append(ParserSonderzeichenString(html))
                break
            if element.start > 0:
                items.append(ParserSonderzeichenString(html[:element.start]))
            items.append(HTMLParser.__create_parser_item(element))
            html = html[element.end + 1:]
        return ParserCollection(items)
