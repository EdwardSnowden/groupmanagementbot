"""Unittests for the module parser."""

import unittest

from ddt import data, ddt, unpack

from .parser import HTMLParser, IParserItem, ParserCollection


class TestIParserItem(unittest.TestCase):
    """Unittests for the interface IParserItem."""
    def test_raises(self):
        """Tests if the interface raises not implemented."""
        item = IParserItem()
        with self.assertRaises(NotImplementedError):
            len(item)
        with self.assertRaises(NotImplementedError):
            str(item)
        with self.assertRaises(NotImplementedError):
            _ = item[:0]
        with self.assertRaises(NotImplementedError):
            item.without_tags()


@ddt
class TestParser(unittest.TestCase):
    """Unittests for the class Parser."""
    @data(
        ("Hallo Welt", 10),
        ("Hallo <b>Welt</b>", 10),
        ("Hallo <strong>Welt</strong>", 10),
        ("Hallo <i>Welt</i>", 10),
        ("Hallo <em>Welt</em>", 10),
        ("Hallo <s>Welt</s>", 10),
        ("Hallo <strike>Welt</strike>", 10),
        ("Hallo <u>Welt</u>", 10),
        ("Hallo <code>Welt</code>", 10),
        ("Hallo <pre>Welt</pre>", 10),
        ('Hallo <a href="http://beispiel.com">Welt</a>', 10),
        ("&lt;", 1),
        ("&gt;", 1),
        ("&amp;", 1),
        ("&quot;", 1),
    )
    @unpack
    def test_len(self, string, length):
        """Tests the correct length of the parsed strings for all tags."""
        parsed = HTMLParser.parse(string)
        self.assertEqual(len(parsed), length)

    @data(
        "Hallo Welt",
        "Hallo <b>Welt</b>",
        "Hallo <i>Welt</i>",
        "Hallo <s>Welt</s>",
        "Hallo <u>Welt</u>",
        "Hallo <code>Welt</code>",
        "Hallo <pre>Welt</pre>",
        'Hallo <a href="http://beispiel.com">Welt</a>',
        "&lt;",
        "&gt;",
        "&amp;",
        "&quot;",
        "hi&lt;",
        "hi&gt;",
        "hi&amp;",
        "hi&quot;",
        "hi&lt;hi",
        "hi&gt;hi",
        "hi&amp;hi",
        "hi&quot;hi",
    )
    def test_to_str(self, string):
        """Tests if the to string works correctly for all tags that are'n replaced."""
        parsed = HTMLParser.parse(string)
        self.assertEqual(str(parsed), string)

    @data(
        ("Hallo <strong>Welt</strong>", "Hallo <b>Welt</b>"),
        ("Hallo <em>Welt</em>", "Hallo <i>Welt</i>"),
        ("Hallo <strike>Welt</strike>", "Hallo <s>Welt</s>"),
    )
    @unpack
    def test_to_str_changed(self, string, string2):
        """Tests if the to string works correctly for all tags that are replaced."""
        parsed = HTMLParser.parse(string)
        self.assertEqual(str(parsed), string2)

    @data(
        ("Hallo Welt", "Hallo Welt"),
        ("Hallo <b>Welt</b>", "Hallo Welt"),
        ("Hallo <strong>Welt</strong>", "Hallo Welt"),
        ("Hallo <i>Welt</i>", "Hallo Welt"),
        ("Hallo <em>Welt</em>", "Hallo Welt"),
        ("Hallo <s>Welt</s>", "Hallo Welt"),
        ("Hallo <strike>Welt</strike>", "Hallo Welt"),
        ("Hallo <u>Welt</u>", "Hallo Welt"),
        ("Hallo <code>Welt</code>", "Hallo Welt"),
        ("Hallo <pre>Welt</pre>", "Hallo Welt"),
        ('Hallo <a href="http://beispiel.com">Welt</a>', "Hallo Welt"),
        ("&lt;", "<"),
        ("&gt;", ">"),
        ("&amp;", "&"),
        ("&quot;", '"'),
    )
    @unpack
    def test_without_tags(self, string, string2):
        """Tests the method without_tags for all tags."""
        parsed = HTMLParser.parse(string)
        self.assertEqual(parsed.without_tags(), string2)

    @data(
        "Hallo Welt",
        "Hallo <b>Welt</b>",
        "Hallo <strong>Welt</strong>",
        "Hallo <i>Welt</i>",
        "Hallo <em>Welt</em>",
        "Hallo <s>Welt</s>",
        "Hallo <strike>Welt</strike>",
        "Hallo <u>Welt</u>",
        "Hallo <code>Welt</code>",
        "Hallo <pre>Welt</pre>",
        'Hallo <a href="http://beispiel.com">Welt</a>',
        "&lt;",
        "&gt;",
        "&amp;",
        "&quot;",
    )
    def test_getitem(self, string):
        """Tests the getitems method."""
        for i in range(0, len(string) + 1):
            sub = string[:i]
            self.assertEqual(len(sub), i)
            sub = string[i:]
            self.assertEqual(len(sub), len(string) - i)

    def test_parser_collection_init_type_error(self):
        """Tests if the parser collection raises an type error."""
        with self.assertRaises(TypeError):
            ParserCollection(7)

    def test_parser_collection_getitem_errors(self):
        """Tests if the parser collection extend works."""
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("<b>Hallo</b>")["hallo"]
        with self.assertRaises(NotImplementedError):
            _ = HTMLParser.parse("<b>Hallo</b>")[7]
        with self.assertRaises(NotImplementedError):
            _ = HTMLParser.parse("<b>Hallo</b>")[-8:]
        with self.assertRaises(NotImplementedError):
            _ = HTMLParser.parse("<b>Hallo</b>")[:8:3]

    def test_parser_sonderzeichen_getitem_errors(self):
        """Tests if the parser collection extend works."""
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("&gt;").items[0].items[0]["hallo"]
        with self.assertRaises(NotImplementedError):
            _ = HTMLParser.parse("&gt;").items[0].items[0][7]

    def test_parser_find_first_tag_type_error(self):
        """Tests if the parser collection extend works."""
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("5 ist < 8")
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("5 ist > 8")
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("<b>hallo")
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse("<b>hallo</b")

    def test_parser_create_parser_item_errors(self):
        """Tests if the parser collection extend works."""
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse('<a>hallo</a>')
        with self.assertRaises(TypeError):
            _ = HTMLParser.parse('<o>hallo</o>')
