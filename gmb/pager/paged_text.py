"""Module that contains classes to split messages. The classes should combine page
splitting methods depending on the message."""

from enum import Enum
from typing import List, Union

from telegram import ParseMode

from .page_splitting_methods import (IPageSplittingMethod, PageSplitAlgo1,
                                     PageSplitByCharLength,
                                     PageSplitByCharLengthHTML)


class PageSize(Enum):
    """Char size of a page."""
    VERY_SMALL = 256
    SMALL = 512
    MIDDLE = 1024
    BIG = 2048
    MAX = 4098


class AbstractPagedText:
    """Splits a text into pages."""
    def __init__(self, text: str, page_splitting_method: IPageSplittingMethod):
        """Creates a AbstractPagedText."""
        self.__pages: List[str] = page_splitting_method.gen_paged_list(text)

    def __getitem__(self, index: int) -> str:
        """Returns the page with the index.

        Index starts counting the first page by 0.

        Raises: IndexError if list index is out of range
        """
        return self.__pages[index]

    def __len__(self):
        """Returns the amount of pages."""
        return len(self.__pages)


class CharLengthPagedText(AbstractPagedText):
    """Pages are the maximum amount of chars that fits into a telegram message."""
    def __init__(self,
                 text: str,
                 parse_mode: ParseMode = ParseMode.HTML,
                 page_size: Union[PageSize, int] = PageSize.MIDDLE):
        """Creates a CharLengthPagedText."""
        if isinstance(page_size, PageSize):
            len_text = page_size.value
        else:
            len_text = page_size
        page_splitting_method = PageSplitAlgo1(len_text)
        # if parse_mode is not ParseMode.HTML:
        #     page_splitting_method: IPageSplittingMethod = PageSplitByCharLength(len_text)
        # else:
        #     page_splitting_method = PageSplitByCharLengthAndWordHTML(len_text)
        super().__init__(text, page_splitting_method)
