from typing import Any, Dict, List


class Extensions:
    """A abstract static list to load extensions on startup.

    This class is used to add extensions (classes or functions) to a static
    list. You can add extensions on startup to the list and during runtime
    access all added extensions. Every inherited class gets its own list of
    extensions.
    """

    __extensions: Dict[Any, List[Any]] = {}

    @classmethod
    def get_extensions(cls):
        """Returns the list of extensions."""
        extensions = Extensions.__extensions
        if cls in extensions.keys():
            return extensions[cls]
        extensions[cls] = []
        return extensions[cls]

    @classmethod
    def add_extension(cls, extension: Any):
        """Adds an extension to the list if it is not already added."""
        extensions = Extensions.__extensions
        if cls in extensions.keys():
            if extension not in extensions[cls]:
                extensions[cls].append(extension)
        else:
            extensions[cls] = [extension]
