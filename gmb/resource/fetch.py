from typing import List, Type

import telegram
from telegram import Bot

from gmb.api.datastructs import AbstractChatsConfig
from gmb.environment import Environment
from gmb.status import DestructionStatus

from .extension import ResourceExt
from .extensions import ResourceExtensions

__locked_fetch = False


def __fetch_single(bot: Bot, extension: Type[ResourceExt],
                   chat_config: AbstractChatsConfig) -> None:
    try:
        print("getting info from chat: {} ({}) (ext: {})".format(
            chat_config.id, chat_config.name, extension.__name__))
        conf = Environment.get().static_config
        extension.factory(bot, extension, chat_config, conf.output_dir)
    except telegram.error.BadRequest:
        print("Error: invalid chat_id " + str(chat_config.id))
        return
    except telegram.error.TelegramError:
        print("Error: telegram error " + str(chat_config.id))
        return


def fetch_all(bot: Bot) -> None:
    global __locked_fetch
    if __locked_fetch:
        print("Notice: Locked fetch")
        return
    __locked_fetch = True
    try:
        conf = Environment.get().static_config
        status = DestructionStatus(
            conf.status_dir,
            filename=conf.infofetch_status_fetching_filename,
            on_creation="1",
            on_deletion="0")
        extensions: List[
            Type[ResourceExt]] = ResourceExtensions.get_extensions()
        for extension in extensions:
            chats_config_class = extension.get_chats_config_class()
            chats_config = chats_config_class.get_all()
            for chat_config in chats_config:
                __fetch_single(bot, extension, chat_config)
        del status
    finally:
        __locked_fetch = False
