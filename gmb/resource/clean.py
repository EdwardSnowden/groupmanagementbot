from typing import List, Type

from telegram import Bot

from .extension import ResourceExt
from .extensions import ResourceExtensions

__locked_clean = False


def __clean_single(extension: Type[ResourceExt], bot: Bot):
    print("Cleaning db: ResourceExt: {}".format(extension.__name__))
    config = extension.get_chats_config_class()
    chat = extension.get_chat_class()
    user = extension.get_user_class()
    chatadmin = extension.get_chat_admin_class()
    # all chat ids that should not be deleted
    q_ids = config.select(config.id)
    # delete chat admins
    chatadmin.delete().where(chatadmin.chat.not_in(q_ids)).execute()
    # delete chats
    chat.delete().where(chat.id.not_in(q_ids)).execute()
    # all user ids that should not be deleted
    q_user_ids = chatadmin.select(chatadmin.admin_id)
    # delete users
    user.delete().where(user.id.not_in(q_user_ids)).execute()


def clean_all(bot: Bot) -> None:
    global __locked_clean
    if __locked_clean:
        print("Notice: Locked clean")
        return
    __locked_clean = True
    try:
        extensions: List[
            Type[ResourceExt]] = ResourceExtensions.get_extensions()
        for extension in extensions:
            __clean_single(extension, bot)
    finally:
        __locked_clean = False
