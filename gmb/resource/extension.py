from __future__ import annotations

from enum import auto
from typing import Type

from telegram import Bot

from gmb.datastructs.model import (AbstractChat, AbstractChatAdmin,
                                   AbstractChatsConfig, AbstractUser)
from gmb.util import AutoInt


class ChatType(AutoInt):
    INTERN = auto()
    EXTERN = auto()


class ResourceExt:
    @staticmethod
    def get_chats_config_class() -> Type[AbstractChatsConfig]:
        """Returns the ChatsConfig Class."""
        raise NotImplementedError()

    @staticmethod
    def get_chat_class() -> Type[AbstractChat]:
        """Returns the Chat Class."""
        raise NotImplementedError()

    @staticmethod
    def get_user_class() -> Type[AbstractUser]:
        """Returns the User Class."""
        raise NotImplementedError()

    @staticmethod
    def get_chat_admin_class() -> Type[AbstractChatAdmin]:
        """Returns the ChatAdmin Class."""
        raise NotImplementedError()

    @staticmethod
    def get_chat_type() -> ChatType:
        """The type chats of this extension belongs to.

        It is used to differentiate between for example partner chats (EXTERN) or chats
        that belong to the network (INTERN) the bot depends to.
        """
        raise NotImplementedError()

    @staticmethod
    def factory(bot: Bot, resource_ext: Type[ResourceExt],
                chats_config: AbstractChatsConfig,
                output_dir: str) -> AbstractChat:
        """Returns the factory method that creates the chats, users and chatadmins objects."""
        raise NotImplementedError()

    @staticmethod
    def subscribed_global_ban(chat_config: AbstractChatsConfig) -> bool:
        """Returns true if a global ban should ban in this chat too."""
        raise NotImplementedError()
