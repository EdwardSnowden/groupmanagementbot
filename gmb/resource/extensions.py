from typing import List, Optional, Type

from gmb.abstract_extensions import Extensions
from gmb.api.datastructs import AbstractChat, AbstractChatsConfig, AbstractChatAdmin

from .extension import ChatType, ResourceExt


class ResourceExtensions(Extensions):
    pass


class Chats:
    @staticmethod
    def get_chats_configs(filter: Optional[ChatType] = None
                          ) -> List[AbstractChatsConfig]:
        chats: List[AbstractChatsConfig] = []
        for extension in ResourceExtensions.get_extensions():
            if filter:
                if extension.get_chat_type() == filter:
                    chats.extend(extension.get_chats_config_class().get_all())
            else:
                chats.extend(extension.get_chats_config_class().get_all())
        return chats

    @staticmethod
    def get_chats(filter: Optional[ChatType] = None) -> List[AbstractChat]:
        chats: List[AbstractChat] = []
        for extension in ResourceExtensions.get_extensions():
            if filter:
                if extension.get_chat_type() == filter:
                    chats.extend(extension.get_chat_class().get_all())
            else:
                chats.extend(extension.get_chat_class().get_all())
        return chats

    @staticmethod
    def get_chat_admin(chatid, userid) -> Optional[AbstractChatAdmin]:
        for extension in ResourceExtensions.get_extensions():
            chat_admin_cls = extension.get_chat_admin_class()
            admin = chat_admin_cls.get_or_none(chat_admin_cls.admin_id == userid,
                                               chat_admin_cls.chat_id == chatid)
            if admin is not None:
                return admin
        return None

    # @staticmethod
    # def get_chat_by_config(config: AbstractChatsConfig
    #                        ) -> Optional[AbstractChat]:
    #     # TODO: funktioniert das hier überhaupt?
    #     return AbstractChat.try_get_by_id(config.id)

    @staticmethod
    def try_get_chat_by_id(cid: int) -> Optional[AbstractChat]:
        for extension in ResourceExtensions.get_extensions():
            for chat in extension.get_chat_class().get_all():
                if chat.id == cid:
                    return chat
        return None

    # @staticmethod
    # def try_get_chats_config_by_id(cid: int) -> Optional[AbstractChatsConfig]:
    #     for extension in ResourceExtensions.get_extensions():
    #         for chat in extension.get_chats_config_class().get_all():
    #             if chat.id == cid:
    #                 return chat
    #     return None

    @staticmethod
    def subscribed_global_ban(cid: int) -> Optional[bool]:
        for extension in ResourceExtensions.get_extensions():
            for chat in extension.get_chats_config_class().get_all():
                if chat.id == cid:
                    return extension.subscribed_global_ban(chat)
        return None

    @staticmethod
    def print_chats() -> None:
        for extension in ResourceExtensions.get_extensions():
            chats = extension.get_chats_config_class().get_all()
            print("{}-Chats:".format(extension.__name__))
            if chats:
                print("\n".join("  {} ({})".format(x.id, x.name)
                                for x in chats))
            else:
                print("  No chats.")


def register_resource_extension(cls: Type[ResourceExt]):
    print("Registered resource extension: {}".format(cls.__name__))
    ResourceExtensions.add_extension(cls)
    return cls
