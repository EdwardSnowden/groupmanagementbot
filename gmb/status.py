from __future__ import annotations

from gmb.util import sanitice_location


class Status:
    def __init__(self: Status, status_dir: str):
        self.status_dir = sanitice_location(status_dir)

    def save_status(self: Status, filename: str, content: str):
        new_filename = self.status_dir + filename
        with open(new_filename, "w") as file:
            file.write(content)


class DestructionStatus(Status):
    def __init__(self: DestructionStatus, status_dir: str, filename: str,
                 on_creation: str, on_deletion: str):
        super().__init__(status_dir)
        self.filename = filename
        self.on_deletion = on_deletion
        self.save_status(filename, on_creation)

    def __del__(self: DestructionStatus):
        self.save_status(self.filename, self.on_deletion)
