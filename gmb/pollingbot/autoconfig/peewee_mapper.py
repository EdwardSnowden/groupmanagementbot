from distutils.util import strtobool
from typing import Type, Optional, Callable, Any, List, Tuple

from peewee import AutoField, BigAutoField, IntegerField, BigIntegerField, SmallIntegerField, IdentityField, FloatField, \
    DoubleField, DecimalField, CharField, FixedCharField, TextField, BlobField, BitField, BigBitField, UUIDField, \
    BinaryUUIDField, DateTimeField, DateField, TimestampField, TimeField, IPField, BooleanField, BareField, \
    ForeignKeyField
from telegram import Bot, Update
from telegram.ext import CommandHandler

from gmb.custom_message.utils import send_custom_reply
from gmb.database.basemodel import BaseModel
from gmb.permissions import only_botadmins
from gmb.pollingbot.extension import PollBotExt, HandlerPriority, PriorityHandler
from gmb.telegram_util import reset_config_member, set_config_member, toggle_config_member, hard_set_config_member


class IConfigEditor(PollBotExt):
    pass


class ConfigEditorBase(IConfigEditor):
    def __init__(self, config_cls: Type[BaseModel], attribute: str, reply_text_prefix: str,
                 command_name: Optional[str] = None,
                 permission_decorator=only_botadmins,
                 priority: HandlerPriority = HandlerPriority.NORMAL,
                 cast_method: Optional[Callable[[str], Any]] = None):
        self.config_cls = config_cls
        self.attribute = attribute
        self.reply_text_prefix = reply_text_prefix
        if command_name and " " in command_name:
            raise Exception("Illegal command name!")
        self.command_name = command_name
        self.permission_decorator = permission_decorator
        self.priority = priority
        self.cast_method = cast_method


class ConfigEditor(ConfigEditorBase):
    def reset_method(self):
        return self.permission_decorator(self.__reset_method)

    def __reset_method(self, bot: Bot, update: Update) -> None:
        reset_config_member(self.config_cls, self.attribute)
        msg = "{} resetted.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def set_method(self):
        return self.permission_decorator(self.__set_method)

    def __set_method(self, bot: Bot, update: Update) -> None:
        set_config_member(self.config_cls, self.attribute, update, cast_method=self.cast_method)
        msg = "{} updated.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def get_handlers(self) -> List[PriorityHandler]:
        return [
            PriorityHandler(
                CommandHandler('set_{}'.format(self.command_name or self.attribute), self.set_method()),
                self.priority),
            PriorityHandler(
                CommandHandler('reset_{}'.format(self.command_name or self.attribute), self.reset_method()),
                self.priority),
        ]


class BooleanConfigEditor(ConfigEditorBase):
    def reset_method(self):
        return self.permission_decorator(self.__reset_method)

    def __reset_method(self, bot: Bot, update: Update) -> None:
        val = reset_config_member(self.config_cls, self.attribute)
        if val:
            msg = "{} is enabled.".format(self.reply_text_prefix)
        else:
            msg = "{} is disabled.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def toggle_method(self):
        return self.permission_decorator(self.__toggle_method)

    def __toggle_method(self, bot: Bot, update: Update) -> None:
        val = toggle_config_member(self.config_cls, self.attribute)
        if val:
            msg = "{} is enabled.".format(self.reply_text_prefix)
        else:
            msg = "{} is disabled.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def enable_method(self):
        return self.permission_decorator(self.__enable_method)

    def __enable_method(self, bot: Bot, update: Update) -> None:
        hard_set_config_member(self.config_cls, self.attribute, True)
        msg = "{} is enabled.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def disable_method(self):
        return self.permission_decorator(self.__disable_method)

    def __disable_method(self, bot: Bot, update: Update) -> None:
        hard_set_config_member(self.config_cls, self.attribute, False)
        msg = "{} is disabled.".format(self.reply_text_prefix)
        send_custom_reply(msg, bot, update)

    def get_handlers(self) -> List[PriorityHandler]:
        tm = self.toggle_method()
        return [
            PriorityHandler(
                CommandHandler('toggle_{}'.format(self.command_name or self.attribute), tm),
                self.priority),
            PriorityHandler(
                CommandHandler('reset_{}'.format(self.command_name or self.attribute), self.reset_method()),
                self.priority),
            PriorityHandler(
                CommandHandler('enable_{}'.format(self.command_name or self.attribute), self.enable_method()),
                self.priority),
            PriorityHandler(
                CommandHandler('disable_{}'.format(self.command_name or self.attribute), self.disable_method()),
                self.priority),
        ]


not_implemented_cast = object()


peewee_mapper = {
    AutoField: int,
    BigAutoField: int,
    IntegerField: int,
    BigIntegerField: int,
    SmallIntegerField: int,
    IdentityField: not_implemented_cast,
    FloatField: float,
    DoubleField: float,
    DecimalField: float,
    CharField: str,
    FixedCharField: str,
    TextField: str,
    BlobField: not_implemented_cast,
    BitField: not_implemented_cast,
    BigBitField: not_implemented_cast,
    UUIDField: not_implemented_cast,
    BinaryUUIDField: not_implemented_cast,
    DateTimeField: not_implemented_cast,
    DateField: not_implemented_cast,
    TimeField: not_implemented_cast,
    TimestampField: not_implemented_cast,
    IPField: not_implemented_cast,
    BooleanField: strtobool,
    BareField: not_implemented_cast,
    ForeignKeyField: not_implemented_cast,
}


def get_cast_method(config_cls: Type[BaseModel], attribute: str) -> Callable[[str], Any]:
    real_attr = getattr(config_cls, attribute)
    for peewee_type, cast_method in peewee_mapper.items():
        if isinstance(real_attr, peewee_type):
            if cast_method == not_implemented_cast:
                break
            return cast_method
    raise Exception("Error! Not implemented cast is used in auto_config_editor.")


class AutoConfigEditor(IConfigEditor):
    def __init__(self, config_cls: Type[BaseModel], attribute: str, reply_text_prefix: str,
                 command_name: Optional[str] = None, permission_decorator=only_botadmins,
                 priority: HandlerPriority = HandlerPriority.NORMAL):
        cast_meth = get_cast_method(config_cls, attribute)
        if cast_meth == strtobool:
            self.intern = BooleanConfigEditor(config_cls, attribute, reply_text_prefix, command_name,
                                              permission_decorator, priority, cast_meth)
        else:
            self.intern = ConfigEditor(config_cls, attribute, reply_text_prefix, command_name,
                                       permission_decorator, priority, cast_meth)

    def get_handlers(self) -> List[PriorityHandler]:
        return self.intern.get_handlers()
