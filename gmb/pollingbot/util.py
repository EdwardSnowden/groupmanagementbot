from typing import List

from gmb.pollingbot.extension import PollBotExt, PriorityHandler


class PollBotExtMerge(PollBotExt):
    """This class is used to merge many PollBotExt into one class, so that only one class needs to be added
    to @register_pollingbot_extension.
    """
    def extensions(self) -> List[PollBotExt]:
        raise NotImplementedError()

    def get_handlers(self) -> List[PriorityHandler]:
        handlers: List[PriorityHandler] = []
        for config in self.extensions():
            handlers.extend(config.get_handlers())
        return handlers
