# Groupmanagement Bot
This bot is to manage telegram group networks. The source code is available on the [the village gitlab](https://git.thevillage.chat/thevillage/groupmanagementbot) licensed under AGPLv3. A docker image is hostet in the [gitlab container registry](https://git.thevillage.chat/thevillage/groupmanagementbot/container_registry).

![Picture of the Bot](doc/pics/home1.png)
![Picture of the Bot](doc/pics/home2.png)

## Features

General Features:
- [Network Groups List](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Features)
- [Partner Groups List](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Features)
- [Send invite link of chats](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Features#send-invite-link-by-tag)
- [Inline Bot](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Features#inline-bot)
- [Admin levels](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Administration)
- [Ban users in all network groups at once](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Restrictions#network-ban)
- [Subscribe bans in partner groups](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Restrictions#global-ban-subscription)
- [Leave unknown chats](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Configuration#leave-unknown-chats)

Customisation Features:
- [Configure the Bot](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Configuration)
- [Customize Messages](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Configuration#customize)
- [Different Modes of Messages](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Configuration#modes)
- [Blacklist Plugins](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/Configuration#plugin-blacklist)

Features for FFF:
- [Ortsgruppen List](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/User-Features#og-groups)
- [Netiquette](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Features/FFF#netiquette)

## Installation
See the [Getting Started Guide](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Installation/Getting-Started) if you wan't to host the bot.

## Development
For development take a look at the [developer documentation](https://git.thevillage.chat/thevillage/groupmanagementbot/-/wikis/Development/Introduction).

## Contributing
If you wan't to contribute to the project you can read the [contribution guidlines](CONTRIBUTING.md).
