# How to contribute
We would love to have your help. Before you start working however, please read and follow this short guide.

## Reporting Issues
First [search](https://git.thevillage.chat/thevillage/groupmanagementbot/-/issues) for existing issues before creating new ones. Provide as much information as possible. Mention the version of GroupmanagementBot, python and docker you are using, and explain (as detailed as you can) how the problem can be reproduced.

## Code contributions
Found a bug and know how to fix it? Great! Please read on.

### Creating Pull Requests
- Make sure your code passes the unittests beforehand
- Rebase your topic branch on top of the development branch before creating the merge request
- Create a merge request to development branch

### Notices:
- If you add new files, that needs to be in the image, include them in [.dockerignore](.dockerignore)
- If you add new telegram commands, add them to the related [doc/telegram_commands/*.txt](doc/telegram_commands/) file
