start - Welcome message
license - License
privacy - Privacy statement
help - Help message
netiquette - Show the netiquette
version - Shows the version of the bot
id - Shows the id of user, chat and message

list_admins - Lists all admins

groups - List of all Network Groups
partners - List of all Partner Groups
ogs - List of all Ortsgruppen
