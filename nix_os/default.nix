{
  system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
}:


with pkgs.python37Packages;
with import ./common.nix;

buildPythonApplication rec {
  pname = "groupmanagement-bot";
  version = "3.8.0";

  src = ./.;
     
  propagatedBuildInputs = with pythonPackages; dependencies;

  meta = with pkgs.lib;{
      description = "This bot is to manage telegram group networks.
        The source code is available on the the village gitlab licensed under AGPLv3.
        A docker image is hostet on docker hub.";
      homepage = https://git.thevillage.chat/thevillage/groupmanagementbot;
      license = licenses.gpl3;
      #maintainers = with maintainers; [  ]; #TODO: add maintainer
      platforms = platforms.unix;
  };
}
